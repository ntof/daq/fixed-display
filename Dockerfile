FROM node:lts-slim

ARG perms=g-w

WORKDIR /app
ADD . /app
RUN echo 'Running chmod with perms' $perms
RUN chmod -R ${perms} /app
ENV NODE_ENV=production
RUN apt-get update \
    && apt-get install busybox git apt-transport-https --yes \
    && echo 'deb [trusted=yes] https://mro-dev.web.cern.ch/distfiles/debian/stretch mro-main/' >> /etc/apt/sources.list \
    && apt-get update \
    && apt-get install oracle-instantclient19.5-basic libaio1 -y \
    && npm install \
    && npm install oracledb \
    && npm install pm2 && mkdir .pm2 && chmod 777 .pm2 \
    && cd www && npm install \
    && apt-get clean
ENV PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:node_modules/.bin
ENV HOME=/app

CMD pm2-docker -n app --no-autorestart ./src/index.js
EXPOSE 8080/tcp
