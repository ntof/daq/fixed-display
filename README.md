# Fixed Display

This is a Web-Application loaded on fixed display monitor on nTOF Control Room

This application is developped according to our [guidelines](https://mro-dev.web.cern.ch/docs/std/en-smm-apc-web-guidelines.html).

## Build

To build this application application (assuming that Node.js is installed on your machine):

```bash
# Run webpack and bundle things in /dist
npm run build

# Serve pages on port 8080
npm run serve
```

## Deploy

To deploy the example application, assuming that oc is configured and connected
on the proper project:

```bash
cd deploy

# Make sure the auth password referred to is correct
vim config-pass.yml

# Generate configuration from config*.yml files and apply it to a pod
./genconf.js fixed-display-beta.yaml config* | oc apply -f -
```
