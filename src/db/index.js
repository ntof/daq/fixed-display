// @ts-check
const
  { assign, cloneDeep, get, first } = require('lodash'),
  stubSetup = require('./scripts/stub-setup'),
  debug = require('debug')('app:db'),
  express = require('express'),
  bodyParser = require('body-parser'),
  KnexServer = require('@cern/mrest');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 */

const doc = require('./doc');

class Database extends KnexServer {
  /**
   *
   * @param {string} basePath
   * @param {string | number } port
   * @param {mrest.DatabaseDescription} schema
   * @param {mrest.KnexServer.Options} config
   */
  constructor(basePath, port, schema, config) {
    super(config, schema, assign(cloneDeep(doc), { basePath }));
    this.basePath = basePath;
    this.port = port;
    this.schema = schema;
  }

  baseUrl() {
    return `localhost:${this.port}${this.basePath}`;
  }

  router() {
    const router = express.Router();

    // Custom apis
    router.get('/runs/totalProtons', bodyParser.json(),
      KnexServer.ExpressHelpers.wrap(this.getTotalProtons.bind(this)));

    super.register(router);
    return router;
  }

  async createStub() {
    debug('Creating Stub');
    return stubSetup(this.knex);
  }

  /**
   * @param {Request} req
   */
  async getTotalProtons(req) {
    const experiment = get(req.query, 'experiment');
    const runNumberMin = get(req.query, 'runNumberMin');
    const runNumber = get(req.query, 'runNumber');
    const from = get(req.query, 'from');
    const to = get(req.query, 'to');

    const ret = await this.knex('RUNS')
    .sum({ protons: 'RUN_TOTAL_PROTONS' })
    .where('RUN_CASTOR_FOLDER', experiment)
    .andWhere('RUN_NUMBER', '>=', runNumberMin)
    .andWhere('RUN_NUMBER', '<', runNumber)
    .whereBetween('RUN_START', [ from, to ]);

    return get(first(ret), 'protons', 0);
  }
}

module.exports = Database;
