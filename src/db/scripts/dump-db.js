// @ts-check
const
  _ = require('lodash'),
  // @ts-ignore
  config = require('../../../config'),
  knex = require('knex');

/* do not use node-oracledb LOB objects, dump content as a Buffer */
const oracledb = require('oracledb');
oracledb.fetchAsBuffer = [ oracledb.BLOB ];

/**
 * @typedef {{
 *  db: knex.Knex
 *  out: knex.Knex,
 *  history: { [tableName: string]: Set<string> }
 * }} Env
 */

/**
 *
 * @param {knex.Knex} k
 * @param {string} tableName
 * @param {any} row
 */
function sqlInsert(k, tableName, row) {
  row = _.omitBy(row, (value, key) => (key[0] === '_'));
  row = _.mapValues(row,
    (value) => (_.isString(value) ? value.replace(/;\r?\n/g, '; \n') : value));
  return k(tableName).insert(row)
  .toString().replace(/^insert/, 'insert or replace');
}

/**
 *
 * @param {Env} env
 * @param {string} tableName
 * @param {any[]} rows
 * @param {string} rowIdName
 */
function dumpRow(env, tableName, rows, rowIdName) {
  if (!_.get(env, [ 'history', tableName ])) {
    _.set(env, [ 'history', tableName ], new Set());
  }
  const hist = env.history[tableName];

  if (_.isArray(rows)) {
    rows = rows.filter((row) => !hist.has(row[rowIdName]));
    _.forEach(rows, (row) => {
      hist.add(row[rowIdName]);
      console.log(sqlInsert(env.out, tableName, row) + ';');
    });
  }
  else if (_.isObject(rows) && !hist.has(rows[rowIdName])) {
    hist.add(rows[rowIdName]);
    console.log(sqlInsert(env.out, tableName, rows) + ';');
  }
}

/**
 * @param {Env} env
 * @param {number[]} ids
 */
async function dumpUsers(env, ids) {
  await env.db('USERS').select('*').whereIn('USER_ID', ids)
  .then((rows) => dumpRow(env, 'USERS', rows, 'USER_ID'));
}

/** @type {Env} */
const env = {
  // @ts-ignore
  db: knex(config.db),
  // @ts-ignore
  out: knex({ client: 'sqlite3', useNullAsDefault: true }),
  history: {}
};

(async () => {
  await env.db('RUNS').select('*')
  .where('RUN_START', '>=', Date.parse('01 Jan 2017'))
  .where('RUN_START', '<=', Date.parse('01 Jan 2018'))
  .limit(500).orderBy('RUN_NUMBER', 'desc')
  .then(async (runs) => {
    dumpRow(env, 'RUNS', runs, 'RUN_NUMBER');
  });

  await env.db('INSTITUTES').select('*')
  .then((inst) => {
    dumpRow(env, 'INSTITUTES', inst, 'INST_ID');
  });

  await env.db('SHIFTS').select('*')
  .where('SHIFT_DATE', '>=', Date.parse('01 Jan 2018'))
  .where('SHIFT_DATE', '<=', Date.parse('01 Jan 2019'))
  .limit(2000)
  .then(async (shifts) => {
    // Dump Relative Associations
    const assIds = _.uniq(_.map(shifts, 'SHIFT_ASSO_ID'));
    await env.db('ASSOCIATIONS').select('*').whereIn('ASSO_ID', assIds)
    .then(async (ass) => {
      // Get special users (reserved and canceled)
      const specialUserIds = await env.db('USERS').select('*')
      .where('USER_FIRST_NAME', '=', 'Reserved')
      .orWhere('USER_FIRST_NAME', '=', 'Cancelled')
      .then((resSpecialUsers) => _.map(resSpecialUsers, 'USER_ID'));
      // Dump Relative Users
      const userIds = _.map(ass, 'ASSO_USER_ID').concat(specialUserIds);
      await dumpUsers(env, _.uniq(userIds));

      dumpRow(env, 'ASSOCIATIONS', ass, 'ASSO_ID');
    });
    dumpRow(env, 'SHIFTS', shifts, 'SHIFT_ID');
  });

})()
.finally(() => {
  env.db.destroy();
});
