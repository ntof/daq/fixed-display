#!/usr/bin/env node
// @ts-check
/* eslint-disable max-lines */

const
  debug = require('debug')('app:stub'),
  { createGunzip } = require('zlib'),
  readline = require('readline'),
  fs = require('fs'),
  path = require('path'),
  knex = require('knex'),
  { first, omit, map, transform } = require('lodash');

/**
 * @typedef {knex.Knex.TableBuilder} TableBuilder
 */

/**
 * @param {knex.Knex} db
 * @param {string} table
 * @param {(table: TableBuilder) => any} fun
 */
async function createTable(db, table, fun) {
  if (!await db.schema.hasTable(table)) {
    await db.schema.createTable(table, fun);
  }
}

/**
 * @param {knex.Knex} db
 */
async function createSchema(db) {
  debug('Building schema');

  /* tables are kept in the same order than database-miugration scripts */
  await createTable(db, 'USERS', function(table) {
    table.increments('USER_ID');
    table.string('USER_LOGIN', 20).unique()
    .comment('may be null for expired users');
    table.string('USER_FIRST_NAME', 50).notNullable();
    table.string('USER_LAST_NAME', 50).notNullable();
    table.string('USER_EMAIL', 50).notNullable();
    table.integer('USER_STATUS').notNullable();
    table.integer('USER_PRIVILEGES').notNullable();
    table.string('USER_INVOLVMENTS', 4000);

    table.index([ 'USER_LAST_NAME', 'USER_FIRST_NAME' ], 'USER_NAME_IDX');
    table.index([ 'USER_LOGIN' ], 'USER_LOGIN_IDX');
  });

  await createTable(db, 'INSTITUTES', function(table) {
    table.increments('INST_ID');
    table.string('INST_NAME', 255).notNullable();
    table.string('INST_CITY', 50);
    table.string('INST_COUNTRY', 50).notNullable();
    table.string('INST_ALIAS', 50).unique().notNullable();
    table.integer('INST_STATUS').notNullable();

    table.index([ 'INST_STATUS', 'INST_NAME' ], 'INST_STATUS_NAME_IDX');
  });

  await createTable(db, 'ASSOCIATIONS', function(table) {
    table.increments('ASSO_ID');
    table.integer('ASSO_OPER_YEAR').notNullable()
    .references('OPERATION_PERIODS.OPER_YEAR').withKeyName('ASSO_OPER_YEAR_FK');
    table.integer('ASSO_USER_ID').notNullable()
    .references('USERS.USER_ID').withKeyName('ASSO_USER_ID_FK');
    table.integer('ASSO_INST_ID').notNullable()
    .references('INSTITUTES.INST_ID').withKeyName('ASSO_INST_ID_FK');

    table.unique([ 'ASSO_OPER_YEAR', 'ASSO_USER_ID' ], 'ASSO_OPER_USER_UNIQ');
    table.index('ASSO_USER_ID', 'ASSO_USER_IDX');
    table.index([ 'ASSO_OPER_YEAR', 'ASSO_INST_ID' ], 'ASSO_YEAR_INST_IDX');
  });

  await createTable(db, 'SHIFTS', function(table) {
    table.increments('SHIFT_ID');
    table.integer('SHIFT_DATE').notNullable();
    table.integer('SHIFT_ASSO_ID')
    .references('ASSOCIATIONS.ASSO_ID').withKeyName('SHIFT_ASSO_ID_FK');
    table.string('SHIFT_SHK_KIND', 35).notNullable()
    .references('SHIFT_KINDS.SHK_KIND').withKeyName('SHIFT_SHK_KIND_FK');

    table.unique([ 'SHIFT_DATE', 'SHIFT_SHK_KIND' ], 'SHIFT_DATE_KIND_UNIQ');
    table.index([ 'SHIFT_DATE', 'SHIFT_ASSO_ID' ], 'SHIFT_DATE_ASSO_IDX');
  });

  await createTable(db, 'RUNS', function(table) {
    table.integer('RUN_NUMBER').primary();
    table.integer('RUN_START').notNullable();
    table.integer('RUN_STOP');
    table.string('RUN_TITLE', 255).notNullable();
    table.string('RUN_DESCRIPTION', 4000);
    table.string('RUN_TITLE_ORIGINAL', 255);
    table.string('RUN_DESCRIPTION_ORIGINAL', 4000);
    table.string('RUN_CASTOR_FOLDER', 255).notNullable();
    table.integer('RUN_TOTAL_PROTONS').defaultTo(0);
    table.string('RUN_RUN_STATUS', 12).notNullable();

    table.string('RUN_DATA_STATUS', 12).notNullable();

    table.integer('RUN_DET_SETUP_ID');

    table.integer('RUN_MAT_SETUP_ID');

    table.index([ 'RUN_START' ], 'RUN_START_IDX');
    table.index([ 'RUN_TITLE' ], 'RUN_TITLE_IDX');
  });
}

/**
 * @param {knex.Knex} db
 * @param {string} file
 */
async function insertData(db, file) {
  const gz = createGunzip();
  fs.createReadStream(file).pipe(gz);

  var trx = await db.transaction();
  const reader = readline.createInterface({
    input: gz, terminal: false
  });
  var i = 0;
  var stmt = '';

  for await (const line of reader) {
    if (line.length <= 0) {
      continue;
    }
    else if (line[line.length - 1] === ';') {
      stmt = stmt + line.slice(0, -1);
    }
    else {
      stmt += line.replace(/; $/g, ';') + '\n';
      continue;
    }
    await trx.raw(stmt);
    stmt = '';

    if ((++i % 1000) === 0) {
      debug('rows inserted:', i);
      await trx.commit();
      trx = await db.transaction();
    }
  }
  debug('rows inserted:', i);
  await trx.commit();
}

/**
 * @param {knex.Knex} db
 */
async function updateData(db) {
  /* update stubs to always work properly :) */
  const today = new Date();
  const thisYear = today.getFullYear();
  const thisMonth = today.getMonth();

  await db.raw(db('USERS').insert({
    USER_EMAIL: 'nowhere@nowhere.com', USER_FIRST_NAME: 'ntofdev',
    USER_ID: 4242, USER_LAST_NAME: 'APC', USER_LOGIN: 'ntofdev',
    USER_PRIVILEGES: 0, USER_STATUS: 1
  }).toString().replace(/^insert/, 'insert or replace'));

  // For Shifts: Copy October 2018 to current month.
  // Delete SHIFTS starting from Jan 2021
  await db('SHIFTS').select('*')
  .where('SHIFT_DATE', '>=', new Date(2021, 0))
  .del();
  // Copy SHIFTS of October 2018 to current month and create new empty one also
  const dateDiff = new Date().setDate(1) - Date.parse('01 Oct 2018');
  await db('SHIFTS').select('*')
  .where('SHIFT_DATE', '>=', Date.parse('01 Oct 2018'))
  .where('SHIFT_DATE', '<', Date.parse('01 Nov 2018'))
  .then(async (shifts) => {
    const newShifts = map(shifts, (s) => {
      s['SHIFT_ID'] += 1000; // Avoid duplicates
      s['SHIFT_DATE'] += dateDiff;
      return s;
    });
    await db('SHIFTS').insert(newShifts);
    // Remove all extra slot of next month (it happen for 30th months and feb)
    await db('SHIFTS').select('*')
    .where('SHIFT_DATE', '>=',
      new Date(today.getFullYear(), today.getMonth() + 1)).del();
    // New empty slots
    const nextMonthDiff = new Date().setMonth(thisMonth + 1) - today.getTime();
    const newEmptyShifts = map(shifts, (s) => {
      s['SHIFT_ID'] += 1000; // Avoid duplicates
      s['SHIFT_DATE'] += nextMonthDiff; // = 2 month;
      s['SHIFT_ASSO_ID'] = null;
      return s;
    });
    await db('SHIFTS').insert(newEmptyShifts);
  });

  // Create Association for current year for Reserved and Cancelled fake users
  await db('USERS').select('*')
  .where('USER_FIRST_NAME', '=', 'Reserved')
  .orWhere('USER_FIRST_NAME', '=', 'Cancelled')
  .then(async (resSpecialUsers) => {
    const specialUserIds = map(resSpecialUsers, 'USER_ID');
    const ntofInstID = await db('INSTITUTES').select('*')
    .where('INST_ALIAS', '=', '__nTOF')
    .then((inst) => map(inst, 'INST_ID'));
    if (specialUserIds.length !== 2 || ntofInstID.length !== 1) {
      debug("ERROR creating special associations for reserved and cancelled");
      return;
    }
    /** @type any[] */
    const specialAssociations = transform(specialUserIds,
      function(/** @type any[] */ res, userId) {
        res.push({
          'ASSO_OPER_YEAR': thisYear,
          'ASSO_USER_ID': userId,
          'ASSO_INST_ID': first(ntofInstID)
        });
      }, []);

    await db.raw(db('ASSOCIATIONS').insert(specialAssociations)
    .toString().replace(/^insert/, 'insert or replace'));
  });

  debug('stub data udpated');
}

/**
 * @param {knex.Knex} db
 */
async function stubSetup(db) {
  await createSchema(db);
  await insertData(db, path.join(__dirname, 'sample.sql.dump.gz'));
  await updateData(db);
}

// @ts-ignore
if (!module.parent) {
  const config = require('../../config-stub'); // eslint-disable-line global-require
  // @ts-ignore
  const db = knex(omit(config.db, 'definition'));

  stubSetup(db)
  .catch((err) => console.log('Error:', err))
  .finally(() => db.destroy());
}
else {
  /* export our server */
  module.exports = stubSetup;
}
