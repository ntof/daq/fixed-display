// @ts-check
/* eslint-disable max-lines */

/** @type {mrest.DatabaseDescription} */
module.exports = {
  runs: {
    table: "RUNS",
    key: "RUN_NUMBER",
    mapping: {
      RUN_NUMBER: "runNumber",
      RUN_START: "start",
      RUN_STOP: "stop",
      RUN_TITLE: "title",
      RUN_DESCRIPTION: "description",
      RUN_TITLE_ORIGINAL: "titleOriginal",
      RUN_DESCRIPTION_ORIGINAL: "descOriginal",
      RUN_CASTOR_FOLDER: "castorFolder",
      RUN_TOTAL_PROTONS: "totalProtons",
      RUN_RUN_STATUS: "runStatus",
      RUN_DATA_STATUS: "dataStatus",
      RUN_DET_SETUP_ID: "detSetupId",
      RUN_MAT_SETUP_ID: "matSetupId"
    },
    strict: true,
    related: {
      castorFiles: {
        path: "castorFiles",
        key: "runNumber"
      },
      documents: {
        path: "documents",
        join: {
          table: "REL_DOCS_RUNS",
          key: "RUN_NUMBER",
          relKey: "DOC_ID"
        }
      },
      comments: {
        path: "comments",
        key: "runNumber"
      },
      hvParameters: {
        path: "hvParameters",
        key: "runNumber"
      },
      triggers: {
        path: "triggers",
        key: "runNumber"
      },
      configurations: {
        path: "configurations",
        key: "runNumber"
      }
    }
  },
  users: {
    table: "USERS",
    key: "USER_ID",
    mapping: {
      USER_ID: "id",
      USER_LOGIN: "login",
      USER_FIRST_NAME: "firstName",
      USER_LAST_NAME: "lastName",
      USER_EMAIL: "email",
      USER_STATUS: "status",
      USER_PRIVILEGES: "privileges",
      USER_INVOLVMENTS: "involvments"
    },
    strict: true,
    related: {
      comments: {
        path: "comments",
        key: "userId"
      },
      associations: {
        path: "associations",
        key: "userId"
      }
    }
  },
  institutes: {
    table: "INSTITUTES",
    key: "INST_ID",
    mapping: {
      INST_ID: "id",
      INST_NAME: "name",
      INST_CITY: "city",
      INST_COUNTRY: "country",
      INST_ALIAS: "alias",
      INST_STATUS: "status"
    },
    strict: true,
    related: {
      associations: {
        path: "associations",
        key: "institute"
      },
      duePoints: {
        path: "duePoints",
        key: "institute"
      }
    }
  },
  shifts: {
    table: "SHIFTS",
    key: "SHIFT_ID",
    mapping: {
      SHIFT_ID: "id",
      SHIFT_DATE: "date",
      SHIFT_ASSO_ID: "assoId",
      SHIFT_SHK_KIND: "shiftKind"
    },
    strict: true
  },
  associations: {
    table: "ASSOCIATIONS",
    key: "ASSO_ID",
    mapping: {
      ASSO_ID: "id",
      ASSO_OPER_YEAR: "operYear",
      ASSO_USER_ID: "userId",
      ASSO_INST_ID: "institute"
    },
    strict: true,
    related: {
      shifts: {
        path: "shifts",
        key: "assoId"
      }
    }
  }
};
