// @ts-check
const path = require('path');

console.warn('Loading stub configuration');

module.exports = {
  title: 'Fixed-Display',
  basePath: '',
  beta: true,
  db: {
    client: 'sqlite3',
    useNullAsDefault: true,
    connection: { filename: path.join(__dirname, '..', 'db.sqlite') },
    definition: {
      info: {
        title: 'Sample stub DB', version: '1.0.0'
      }
    }
  },
  fixedDisplay: {
    zones: [
      {
        name: "LOCALHOST",
        dns: "127.0.0.1",
        servers: [
          {
            name: "LOCALHOST",
            host: "localhost"
          }
        ]
      },
      {
        name: "LOCALHOST2",
        dns: "127.0.0.1",
        servers: [
          {
            name: "LOCALHOST2",
            host: "localhost"
          }
        ]
      }
    ]
  }
};
