const debug = require('debug')('app:buffer:rfm'),
  { forEach, get, set, transform, bindAll, meanBy } = require('lodash'),
  { TimedBuffer } = require("@cern/buffer-express"),
  { DicXmlDataSet, XmlData } = require("@ntof/dim-xml"),
  { DnsClient } = require("@cern/dim");

const retentionMs = 15 * 60 * 1000; // 15 minutes

// @ts-ignore
const ParamType = XmlData.Type;

class RFMRateStats {
  constructor() {
    this.rate = 0;
    this.rateAvg = 0;
    /** @type {?number} */
    this.timestamp = null;
  }
}

// This is partial
const InfoDatasetMap = {
  rate: { idx: 9, type: ParamType.UINT32 }
};

class RFMRateBuffer extends TimedBuffer {
  /**
   * @param {string} zoneName
   * @param {string} dns
   */
  constructor(zoneName, dns) {
    super(`[${zoneName}]RFMRateBuffer`, { retentionMs });
    this.zoneName = zoneName;
    this.dns = dns;

    bindAll(this, [ 'onError', 'onInfoData' ]);
  }

  connect() {
    if (this.dns) {
      debug('connecting to MERGER/Info with dns:', this.dns);
      this.current = new DicXmlDataSet('MERGER/Info',
        { timeout: 1000 }, new DnsClient(this.dns));
      this.current.on('error', this.onError);
      this.current.on('value', this.onInfoData);
      this.current.promise().catch(this.onError);
    }
    else {
      debug('not connected to MERGER: please provide a dns');
    }
  }

  close() {
    debug("Closing RFMRateBuffer...");
    this.current?.release();
    this.current = null;
  }

  /**
   * @param {any} err
   */
  onError(err) {
    debug("Error received from MERGER/Info service with dns:", this.dns);
    debug("Error: ", err);
  }

  /**
   * @param {?any} data
   */
  onInfoData(data) {
    if (!data) { return; }
    const stats = this.extractInfoData(data);
    const rateAvg = meanBy(this.buffer, 'rate'); // avg of past points
    stats.rateAvg = isNaN(rateAvg) ? 0 : rateAvg;
    this.append(stats); // it will add the timestamp
  }

  /**
   * @param {{ [key: string]: string }} value
   * @returns {RFMRateStats}
   */
  extractInfoData(value) /*: */ {
    /** @type {{ [index: number]: string }} */
    const vals = transform(value, (ret, val) => {
      // @ts-ignore
      ret[val.index] = val;
    }, []);

    const stats = new RFMRateStats();
    forEach(InfoDatasetMap, (param, name) => {
      set(stats, name,
        XmlData.parseValue(get(vals, [ param.idx, 'value' ], null),
          param.type));
    });
    return stats;
  }
}

module.exports = RFMRateBuffer;
