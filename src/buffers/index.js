// @ts-check
const
  { forEach } = require('lodash'),
  debug = require('debug')('app:buffer'),
  express = require('express'),
  RFMRateBuffer = require('./RFMRateBuffer'),
  DiskUsageBuffer = require('./DiskUsageBuffer'),
  { BufferEndpoint } = require('@cern/buffer-express');

/**
 * @typedef {import('../types').Zone} Zone
 */

class Buffers {
  /**
   * @param {Array<Zone>} zones
   */
  constructor(zones) {
    this.zones = zones;

    debug("Creating RFM buffers");
    /** @type Array<RFMRateBuffer> */
    this.rateBuffers = [];
    forEach(this.zones, (zone) => {
      this.rateBuffers.push(new RFMRateBuffer(zone.name, zone.dns));
    });

    debug("Creating Disk buffer");
    this.diskBuffer = new DiskUsageBuffer(this.zones);

    /** @type Array<BufferEndpoint> */
    this.endpoints = [];
  }

  router() {
    debug("Creating router");
    const router = express.Router();

    forEach(this.rateBuffers, (buffer) => {
      const ep = new BufferEndpoint(
        { rw: false, path: `/${buffer.zoneName}/rate`, buffer });
      debug("Registering buffer", buffer.name, ep.path);
      ep.register(router);
      this.endpoints.push(ep);
    });
    const diskEp = new BufferEndpoint(
      { rw: false, path: `/diskUsage`, buffer: this.diskBuffer });
    diskEp.register(router);
    this.endpoints.push(diskEp);

    BufferEndpoint.registerList(router);
    return router;
  }

  connect() {
    forEach(this.rateBuffers, (b) => b.connect());
    this.diskBuffer?.connect();
  }

  close() {
    debug("Closing Buffers...");
    forEach(this.rateBuffers, (b) => b.close());
    this.diskBuffer?.close();
    forEach(this.endpoints, (e) => e.close());
  }

}

module.exports = Buffers;
