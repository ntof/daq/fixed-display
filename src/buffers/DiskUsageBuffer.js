const debug = require('debug')('app:buffer:disk'),
  { forEach, get, set, transform, ceil } = require('lodash'),
  { TimedBuffer } = require("@cern/buffer-express"),
  { DicXmlDataSet, XmlData } = require("@ntof/dim-xml"),
  { DnsClient } = require("@cern/dim");

const retentionMs = 15 * 60 * 1000; // 15 minutes

// @ts-ignore
const ParamType = XmlData.Type;

class DiskStats {
  constructor() {
    this.diskUsed = 0;
  }
}

const SystemSummaryMap = {
  diskUsed: { idx: 0, type: ParamType.DOUBLE }
  // ramUsed: { idx: 1, type: ParamType.DOUBLE },
  // cpuUsed: { idx: 2, type: ParamType.DOUBLE }
};

const DISK_UPDATE_INTERVAL = 2 * 1000; // 2 seconds

/**
 * @typedef {import('../types').Zone} Zone
 */

class DiskUsageBuffer extends TimedBuffer {
  /**
   * @param {Array<Zone>} zones
   */
  constructor(zones) {
    super(`DiskUsageBuffer`, { retentionMs });
    this.zones = zones;
    /** @type Array<DicXmlDataSet> */
    this.clients = [];
    /** @type {{[name: string]: number}} */
    this.lastDiskData = {};

    /** @type {NodeJS.Timer?} */
    this._timer = null;
  }

  connect() {
    debug("connect Disk services");
    forEach(this.zones, (zone) => {
      forEach(zone.servers, (server) => {
        debug(`connecting to: ${server.host}/SystemSummary`);
        const service = new DicXmlDataSet(`${server.host}/SystemSummary`,
          {}, new DnsClient(zone.dns));
        service.on('error', this.onError.bind(this, server.host));
        service.on('value', this.onData.bind(this, server.name));
        service.promise().catch(this.onError.bind(this, server.host));
        // @ts-ignore
        this.clients.push(service);
      });
    });
    debug("start ticker");
    this._timer = setInterval(
      this.pushValuesToBuffer.bind(this),
      DISK_UPDATE_INTERVAL
    );
  }

  pushValuesToBuffer() {
    // debug("append", this.lastDiskData);
    this.append(this.lastDiskData);  // it will add the timestamp
  }

  close() {
    debug("Closing DiskUsageBuffer...");
    // @ts-ignore
    forEach(this.clients, (c) => c.release());
    this.clients = [];
  }

  /**
   * @param {string} host
   * @param {any} err
   */
  onError(host, err) {
    debug("Error received from SystemSummary service of host:", host);
    debug("Error: ", err);
  }

  /**
   * @param {string} name
   * @param {?any} data
   */
  onData(name, data) {
    if (!data) { return; }
    const val = this.extractData(data);
    set(this.lastDiskData, name, ceil(val.diskUsed, 2));
  }

  /**
   * @param {{ [key: string]: string }} value
   * @returns {DiskStats}
   */
  extractData(value) /*: */ {
    /** @type {{ [index: number]: string }} */
    const vals = transform(value, (ret, val) => {
      // @ts-ignore
      ret[val.index] = val;
    }, []);

    const stats = new DiskStats();
    forEach(SystemSummaryMap, (param, name) => {
      set(stats, name,
        XmlData.parseValue(get(vals, [ param.idx, 'value' ], null),
          param.type));
    });
    return stats;
  }
}

module.exports = DiskUsageBuffer;
