const { merge, defaultTo, toString, isNil } = require('lodash');
const { load } = require('@cern/js-yaml');
const fs = require('fs');
const path = require('path');
const debug = require('debug')('yaml:loadAll');

/**
 * @param  {string} dir
 * @param  {RegExp} [pattern=/^config.*.yml$/] [description]
 * @return {AppServer.Config|null}
 */
function loadAll(dir, pattern = /^config.*\.yml$/) {
  try {
    /** @type {string[]} */
    const files = [];
    fs.readdirSync(dir).forEach((f) => {
      if ((f.endsWith('.json') || f.endsWith('.yml')) &&
          (!pattern || pattern.test(f))) {
        files.push(f);
      }
    });
    debug('Loading files: %o from "%s"', files, dir);
    return files.reduce((ret, f) => merge(ret,
      load(toString(fs.readFileSync(path.join(dir, f))))), null);
  }
  catch (e) {
    // @ts-ignore
    debug(e.message);
    return null;
  }
}

/** @type {AppServer.Config | null} */
var config = loadAll('/etc/app');

if (isNil(config)) {
  // Load the stub
  /* eslint-disable-next-line global-require */
  config = /** @type {AppServer.Config} */ (require('./config-stub'));
  config.isStub = true;
}

config.port = defaultTo(config.port, 8080);
config.basePath = defaultTo(config.basePath, '');

module.exports = /** @type {AppServer.Config} */ (config);
