// @ts-check
const
  debug = require('debug')('app:server'),
  express = require('express'),
  helmet = require('helmet'),
  serveStatic = require('express-static-gzip'),
  path = require('path'),
  cors = require('cors'),
  ews = require('express-ws'),
  { attempt, noop, get, flatten, map, transform, set } = require('lodash'),
  { makeDeferred } = require('@cern/prom'),
  { DisProxy, DnsProxy } = require('@ntof/redim-dim'),

  logger = require('./httpLogger'),
  Buffers = require('./buffers'),
  Database = require('./db'),
  schema = require('./db/schema');

/**
 * @typedef {import('express').Request} Request
 * @typedef {import('express').Response} Response
 * @typedef {import('express').NextFunction} NextFunction
 */

class Server {
  /**
   * @param {AppServer.Config} config
   */
  constructor(config) {
    this.config = config;
    this.zones = get(this.config, [ 'fixedDisplay', 'zones' ]);
    this.app = express();
    this._prom = this.prepare(config);
  }

  /**
   * @param {AppServer.Config} config
   */
  async prepare(config) {
    if (process.env.NODE_ENV === 'production') {
      // @ts-ignore
      this.app.use(helmet());
    }
    logger.register(this.app);
    ews(this.app);
    this.router = express.Router();
    const dist = path.join(__dirname, '..', 'www', 'dist');
    this.router.use('/dist', serveStatic(dist, { enableBrotli: true }));

    this.app.set('view engine', 'pug');
    this.app.set('views', path.join(__dirname, 'views'));

    this.router.get('/', (req, res) => res.render('index', config));

    /* NOTE: declare your additional endpoints here */

    // Config
    this.diskAxisDef = this.genDiskAxisDef();
    // @ts-ignore
    this.router.get("/config", cors(), (req, res) => {
      res.json({ zones: this.zones, diskAxisDef: this.diskAxisDef });
    });

    // Buffers
    this.buffers = new Buffers(this.zones);
    this.router.use('/buffers', cors(), this.buffers.router());

    // RedimClient
    DnsProxy.register(this.router);
    DisProxy.register(this.router);

    // DB
    this.db = new Database(config.basePath + '/db',
    // @ts-ignore it defaulted
      config.port, schema, config.db);
    this.router.use('/db', cors(), this.db.router());

    if (config.basePath) {
      this.router.get('/beta',
        // @ts-ignore it defaulted
        (req, res) => res.redirect(path.join('/beta', config.basePath)));
    }

    // @ts-ignore it's defaulted
    this.app.use(config.basePath, this.router);

    // default route
    this.app.use(function(req, res, next) {
      next({ status: 404, message: 'Not Found' });
    });

    // error handler
    this.app.use(function(
      /** @type {any} */ err,
      /** @type {Request} */ req,
      /** @type {Response} */res,
      /** @type {NextFunction} */ next) { /* eslint-disable-line */ /* jshint ignore:line */
      res.locals.message = err.message;
      res.locals.error = req.app.get('env') === 'development' ? err : {};
      res.locals.status = err.status || 500;
      res.status(err.status || 500);
      res.render('error', config);
    });
  }

  connectBuffers() {
    debug("Connecting Buffers...");
    this.buffers?.connect();
  }

  genDiskAxisDef() {
    const servers = flatten(map(this.zones, 'servers'));
    const serverAxisDef = transform(servers, (ret, value) => {
      set(ret, value.name, `%disk of ${value.name}`);
    }, {});
    return serverAxisDef;
  }

  close() {
    debug("Closing Server...");
    this.buffers?.close();
    this.server?.close();
    this.server = null;
  }

  /**
   * @param {() => any} cb
   */
  async listen(cb) {
    await this._prom;
    const def = makeDeferred();
    /* we're called as a main, let's listen */
    var server = this.app.listen(this.config.port, () => {
      this.server = server;
      def.resolve(undefined);
      return attempt(cb || noop);
    });
    return def.promise;
  }

  address() {
    if (this.server) {
      return this.server.address();
    }
    return null;
  }
}

module.exports = Server;
