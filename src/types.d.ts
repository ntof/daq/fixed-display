
import 'mrest';

export = AppServer
export as namespace AppServer

declare namespace AppServer {
    interface Config {
        title: string,
        port?: number,
        basePath?: string,
        db: mrest.KnexServer.Options,
        isStub?: boolean
        fixedDisplay: { zones: Array<Zone> }
    }

    interface Server {
        name: string;
        host: string;
    }

    interface Zone {
        name: string;
        dns: string;
        servers: Array<Server>
    }
}
