// @ts-check

import { TITLE, VERSION } from './Consts';
import Vue from 'vue';
import { get, isNil } from 'lodash';

const component = Vue.extend({
  name: 'App',
  data() { return { TITLE, VERSION }; },
  beforeMount() {
    this.$store.sources.config.fetch();
  },
  mounted() {
    // get parameters about theme
    const theme = get(this.$route.query, 'theme');
    if (!isNil(theme)) {
      this.$store.commit('ui/update', { darkMode: theme === 'dark' });
      this.$store.dispatch('ui/applyTheme');
    }
  }
});

export default component;
