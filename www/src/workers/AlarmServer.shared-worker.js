// @ts-check
import { bindAll, forEach, get, includes,
  isArray,
  set, toNumber, transform, unset, update } from 'lodash';
import { BaseCollectionWorker } from '@cern/base-web-workers';
import { DicXmlValue, xml } from '@ntof/redim-client';

import { Alarm } from '../interfaces/alarmserver';

/**
 * @typedef {import('../interfaces/types').AlarmServer.Stats} Stats
 */

/*::
import type { BaseCollectionWorker$messageIn, BaseCollectionWorker$messageOut } from '@cern/base-web-workers'

export type AlarmServerWorker$messageIn = $Shape<{
  ...BaseCollectionWorker$messageIn,
  service: string,
  options: any,
  ...
}>

export type AlarmServerWorker$messageOut = $Shape<{
  ...BaseCollectionWorker$messageOut,
  stats: AlarmServerWorker$Stats,
  ...
}>
*/


/**
 * @typedef {Partial<{
 *  service: string,
 *  options: any
 * }>} PartMessageIn
 * @typedef {Partial<{
 *  stats: Stats,
 * }>} PartMessageOut
 * @typedef { BaseWorker.BaseCollectionWorker.MessageIn & PartMessageIn} MessageIn
 * @typedef { BaseWorker.BaseCollectionWorker.MessageOut & PartMessageOut} MessageOut
 */
class AlarmServerSharedWorker extends BaseCollectionWorker {
  /**
   * @param {(msg: MessageOut) => any} post
   */
  constructor(post) {
    super(post);
    bindAll(this, [ 'handleError', 'handleValue' ]);
  }

  destroy() {
    this.close();
    super.destroy();
  }

  /**
   * @param  {MessageIn} msg
   * @return {void}
   */
  process(msg) {
    if (!msg) { return; }

    if (msg.service) {
      // prevents filtering to automatically restart
      this.in.data = null;
      unset(this.out, 'stats');
    }
    super.process(msg);

    if (msg.service) {
      this.close();
      this.in.data = null;
      this.client = new DicXmlValue(msg.service, msg.options,
        get(msg.options, 'dns'));
      this.client.on('error', this.handleError);
      this.client.on('value', this.handleValue);
      this.client.promise().catch(this.handleError);
    }
  }

  close() {
    if (this.client) {
      this.client.removeListener('value', this.handleValue);
      this.client.removeListener('error', this.handleError);
      this.client.close();
      // $FlowIgnore
      this.client = null;
      this.abort();
    }
    unset(this.out, 'stats');
  }

  /**
   * @param {any} error
   */
  handleError(error) {
    // $FlowIgnore
    this.post({ error: error });
  }

  /**
   * @param {any} data
   * @returns {Alarm}
   */
  createAlarm(data) {
    const ident = get(data, [ '$', 'ident' ]);
    const alarm = new Alarm(ident);
    forEach(get(data, [ '$' ]), function(value, key) {
      if (includes([ 'active', 'masked', 'disabled' ], key)) {
        value = value === 'true';
      }
      else if (includes([ 'severity', 'date' ], key)) {
        value = value ? toNumber(value) : null;
      }
      set(alarm, key, value);
    });
    set(alarm, 'message', get(data, [ '$textContent' ]));
    return alarm;
  }

  /**
   * @param {any} value
   */
  handleValue(value) {
    const dataJs = xml.toJs(value);
    /* $FlowIgnore*/
    if (!dataJs || !dataJs.alarms) {
      this.handleError("Something went wrong while retrieving files: " + JSON.stringify(dataJs));
      return;
    }

    let data = get(dataJs, 'alarms.alarm');
    if (!isArray(data)) {
      data = [ data ]; // Not an array if is a single alarm
    }
    /** @type Array<Alarm> */
    const ret = transform(data,
      /**
       * @param {Array<Alarm>} ret
       * @param {any} alarmData
       */
      (ret, alarmData) => {
        const alarmObj = this.createAlarm(alarmData);
        ret.push(alarmObj);
      }, []);

    const stats = get(dataJs, 'stats.$');
    // String to number
    forEach(stats, (value, key) => update(stats, key, (n) => toNumber(n)));
    /** @type MessageOut */(this.out).stats = stats;
    this.process({ data: ret });
  }
}

BaseCollectionWorker.create = (p) => new AlarmServerSharedWorker(p);
