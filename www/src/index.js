// @ts-check
import { Settings } from "luxon";
// Configure the time zone
// @ts-ignore
Settings.defaultZoneName = "Europe/Paris";
Settings.defaultLocale = "fr";

import './public-path';
import Vue from 'vue';
import VueRouter from 'vue-router';
// @ts-ignore
import App from './App.vue';
import router from './router';
import BaseVue from '@cern/base-vue';
import NtofVueWidgets from '@ntof/ntof-vue-widgets';
import store from './store';

Vue.use(VueRouter);
Vue.use(BaseVue);
Vue.use(NtofVueWidgets);

export default new Vue({
  el: '#app',
  components: { App },
  store,
  router,
  async beforeCreate() {
    store.dispatch('ticker/startTicker');
  },
  beforeDestroy() {
    store.dispatch('ticker/haltTicker');
  },
  render: (h) => h(App)
});
