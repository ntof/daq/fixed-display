// @ts-check

/* NOTE: declare constants here */

/** @type {string} */
// @ts-ignore
const VER = (typeof VERSION === 'undefined') ? 'unknown' : VERSION; // jshint ignore:line
export { VER as VERSION };

/** @type {string} */
// @ts-ignore
const TI = (typeof TITLE === 'undefined') ? 'unknown' : TITLE; // jshint ignore:line
export { TI as TITLE };

export const TriggerModeMap = {
  0: 'DISABLED',
  1: 'AUTOMATIC',
  2: 'CALIBRATION'
};

export const ShiftKindType = {
  LEADER: 'shift-leader',
  NIGHT: 'night',
  MORNING: 'morning',
  AFTERNOON: 'afternoon'
};

export const AlarmSeverity = {
  WARNING: 2,
  ERROR: 3,
  CRITICAL: 4
};

/** @enum {number} */
export const EacsState = {
  LOADING: 1,
  IDLE: 2,
  FILTERS_INIT: 3,
  SAMPLES_INIT: 4,
  COLLIMATOR_INIT: 5,
  HIGHVOLTAGE_INIT: 6,
  DAQS_INIT: 7,
  MERGER_INIT: 8,
  STARTING: 9,
  RUNNING: 10,
  STOPPING: 11,
  ERROR: -2
};

export const AreaMin = {
  EAR1: 100000,
  EAR2: 200000
};

export const AvgWindowMinutes = 15;
