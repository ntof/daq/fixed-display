/* eslint-disable vue/one-component-per-file */
// @ts-check

import { get, has, isString, lowerFirst, toString } from 'lodash';
import Vue from 'vue';

/** @type {string} */
var _currentUrl = window.location.origin + window.location.pathname;

/**
 * @return {string}
 */
export function currentUrl() {
  return _currentUrl;
}

/**
 * @param {string} url
 */
export function setCurrentUrl(url) {
  _currentUrl = url;
}

/**
 * @param  {string} dns DIM dns hostname
 * @return {string} dns proxy url
 */
export function dnsUrl(dns) {
  return _currentUrl + '/' + dns + '/dns/query';
}

/**
 * @param {string} url
 */
export function httpToWs(url) {
  return url.replace(/^http(s)?\:\/\//, "ws$1://");
}

let id = 0;
/**
 * @return {string}
 */
export function genId() {
  return 'a-' + (++id);
}


export const UrlUtilitiesMixin = Vue.extend({
  methods: {
    getProxy() {
      return _currentUrl;
    },
    currentUrl() {
      return _currentUrl;
    },
    /**
     * @param  {string} dns
     */
    dnsUrl(dns/*: any */) {
      return _currentUrl + '/' + dns + '/dns/query';
    }
  }
});

/**
 * @param  {string} prefix string to append on errors
 * @param  {string|Error} err
 * @return {string|Error}
 */
export function errorPrefix(prefix, err) {
  if (isString(err)) {
    return prefix + err;
  }
  else if (has(err, 'message')) {
    err.message = prefix + lowerFirst(err.message);
  }
  throw err;
}

/**
 * @details this mixin can be used to watch the store for changes
 * @typedef {V.Instance<typeof StoreWatchMixin>} Instance
 */
export const StoreWatchMixin = Vue.extend({
  methods: {
    /**
     * @brief watch the store for changes _and_ get initial value
     * @param  {Array<string>} path
     * @param  {function} fn callback
     * @return {function}
     */
    storeWatch(path, fn) {
      const value = get(this.$store.state, path);
      if (value !== undefined) { fn(value); }
      return this.$store.watch(
        /**
         * @param {AppStore.State} state
         */
        (state) => get(state, path), fn);
    }
  }
});

/**
 * @param  {Blob} filePath
 * @return {Promise<string>}
 */
export const readFileAsText = (filePath) => {
  return new Promise((resolve, reject) => {
    const fr = new FileReader();
    fr.onload = () => resolve(toString(fr.result));
    fr.onerror = reject;
    fr.readAsText(filePath);
  });
};
