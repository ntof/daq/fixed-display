// @ts-check

import { merge } from 'lodash';
import Vuex from 'vuex';
import Vue from 'vue';
import {
  createStore,
  storeOptions } from '@cern/base-vue';

import ticker from './modules/ticker';

import ConfigSource from './sources/ConfigSource';

Vue.use(Vuex);

const fdStore = { state: {}, mutations: {}, modules: {}, plugins: [] };

fdStore.plugins.push(ConfigSource.plugin('config'));
Vue.set(fdStore.modules, 'ticker', ticker);

merge(storeOptions, /** @type {V.StoreOptions<AppStore.State>} */ (fdStore));

const store = /** @type {V.Store<AppStore.State>} */ (createStore());
export default store;
