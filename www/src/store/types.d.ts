import { AlarmServer, Config, DB, EACS, Timing } from "../interfaces/types";
import { DateTime } from 'luxon';
import { RFMRateStats, RFMergerRun } from '../interfaces/rfmerger';

export = AppStore
export as namespace AppStore

declare namespace AppStore {
  // eslint-disable-next-line @typescript-eslint/no-empty-interface
  interface State {
  }

  interface UiState {
    showKeyHints: boolean
  }

  interface Config {
    zones: Array<Config.Zone>;
  }

  interface EacsState {
    strValue?: string;
    value?: number;
    errors?: Array<{ code: number, message: string } & {[id: string]: any}>;
    warnings?: Array<{ code: number, message: string } & {[id: string]: any}>;
    runInfo: EACS.RunInfo;
    daqList: {[id: string]: EACS.DaqListInfo};
    timing: Timing.TimingData;
    lastEvent: EACS.EventData;
  }

  interface AlarmState {
    alarms?: Array<AlarmServer.Alarm> | null,
    stats?: AlarmServer.Stats | null,
    loading: boolean
  }

  interface RFMState {
    current?: RFMergerRun | null,
    buffer: Array<RFMRateStats>,
    lastBufferData: RFMRateStats | null,
  }

  interface DiskUsage {
    buffer: Array<{[name: string]: number}>,
    lastBufferData: {[name: string]: number} | null,
  }

  interface TickerState {
    now: DateTime
  }

  interface ShiftState  {
    todayShifts: {[shiftKind: string]: DB.ShiftInfo};
  }
}
