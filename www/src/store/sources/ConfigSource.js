// @ts-check
import d from 'debug';
import { get, set } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import axios from 'axios';
import { currentUrl } from '../../utilities';
import { makeDeferred } from '@cern/prom';
import AutoRegisterSource from './AutoRegisterSource';

const debug = d('app:store:config');

/**
 * @brief class for getting config
 */
export default class ConfigSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store) {
    super(store, 'config');
    this.store = store;
    this._prom = makeDeferred();
    this.fetch();
  }

  async getConfiguration() {
    const res = await axios.get(currentUrl() + '/config').catch(this.onError);
    return get(res, 'data', []);
  }

  async fetch() {
    this._prom = makeDeferred();
    const config = await this.getConfiguration();
    debug("Config Retrieved.");
    this._commit('update', config);
    this._prom.resolve(config);
  }

  waitForFetch() {
    return this._prom.promise;
  }

  /**
   * @param {any} err
   */
  onError(err) {
    debug("ConfigSource Error:", err);
    logger.error(err);
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   * @param {string} namespace
   */
  static register(store, namespace) {
    AutoRegisterSource.registerModule(store, namespace);
    set(store, [ 'sources', namespace ], new ConfigSource(store));
  }

  /**
   * @brief register module and source
   * @param {string} namespace
   * @return {(store: V.Store<AppStore.State>) => void}
   */
  static plugin(namespace) {
    return function(store) {
      return ConfigSource.register(store, namespace);
    };
  }
}
