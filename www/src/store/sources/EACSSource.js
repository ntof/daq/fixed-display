// @ts-check
import d from 'debug';
import { bindAll, first, forEach, get, keyBy, set, transform } from 'lodash';
import { DicXmlDataSet, DicXmlParams, DicXmlState } from '@ntof/redim-client';
import { BaseLogger as logger } from '@cern/base-vue';
import { currentUrl, dnsUrl } from '../../utilities';
import { DaqListDaqMap, EACSEventsMap, RunInfoMap } from '../../interfaces/eacs';
import { TimingParamsMap } from '../../interfaces/timing';
import { parseDataSet } from '../../interfaces';
import AutoRegisterSource from './AutoRegisterSource';

const debug = d('app:store:eacs');

const MAX_UINT32 = 4294967295;

/**
 * @typedef {import('../../interfaces/types').EACS.RunInfo} RunInfo
 * @typedef {import('../../interfaces/types').EACS.DaqListInfo} DaqListInfo
 * @typedef {import('../../interfaces/types').XML.ParamDesc} ParamDesc
 */

export default class EACSSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   * @param {string} dns
   */
  constructor(store, zone, dns) {
    super(store, EACSSource.getNamespace(zone));
    this.zone = zone;
    this.dns = dns;
    /** @type Array<DicXmlState | DicXmlParams | DicXmlDataSet> */
    this.clients = [];

    bindAll(this, [ 'onError', 'onState', 'onRunInfo', 'onDaqList', 'onTiming', 'onEvent' ]);
    this.connect();
  }

  connect() {
    debug('connecting to EACS:', this.dns);
    this.close();
    if (this.dns) {
      const state = new DicXmlState('EACS/State',
        { proxy: currentUrl() }, dnsUrl(this.dns));
      state.on('error', this.onError);
      state.on('value', this.onState);
      state.promise().catch(this.onError);
      this.clients.push(state);

      const runInfo = new DicXmlParams('EACS/RunInfo',
        { proxy: currentUrl() }, dnsUrl(this.dns));
      runInfo.on('error', this.onError);
      runInfo.on('value', this.onRunInfo);
      runInfo.promise().catch(this.onError);
      this.clients.push(runInfo);

      const daqList = new DicXmlDataSet('EACS/Daq/List',
        { proxy: currentUrl() }, dnsUrl(this.dns));
      daqList.on('error', this.onError);
      daqList.on('value', this.onDaqList);
      daqList.promise().catch(this.onError);
      this.clients.push(daqList);

      const timing = new DicXmlParams('EACS/Timing',
        { proxy: currentUrl() }, dnsUrl(this.dns));
      timing.on('error', this.onError);
      timing.on('value', this.onTiming);
      timing.promise().catch(this.onError);
      this.clients.push(timing);

      const events = new DicXmlDataSet('EACS/Events',
        { proxy: currentUrl() }, dnsUrl(this.dns));
      events.on('error', this.onError);
      events.on('value', this.onEvent);
      events.promise().catch(this.onError);
      this.clients.push(events);
    }
    else {
      this.onRunInfo(null);
      this.onDaqList(null);
    }
  }

  close() {
    forEach(this.clients, (c) => c.close());
    this.clients = [];
  }

  /**
   * @param {any} err
   */
  onError(err) {
    logger.error(err);
  }

  /**
   * @param {any} data
   */
  onState(data) {
    this._commit('state/update', data);
  }

  /**
   * @param {?any} data
   */
  onRunInfo(data) {
    const indexed = keyBy(data, 'index');
    const values = /** @type RunInfo */(transform(RunInfoMap, (ret, param, name) => {
      // @ts-ignore
      ret[name] = get(indexed, [ param.index, 'value' ], null);
    }, {}));
    if (get(values, [ 'eventNumber' ], 0) >= MAX_UINT32) {
      values.eventNumber = null;
    }
    if (get(values, 'validatedNumber', 0) >= MAX_UINT32) {
      values.validatedNumber = null;
    }
    debug('runInfo retrieved');
    this._commit('runInfo/update', values);
  }

  /**
   * @param {any} data
   */
  onDaqList(data) {
    const daqs = /** @type {Object<string, DaqListInfo>} */(transform(data, (ret, daqData) => {
      const daq =  /** @type DaqListInfo */(parseDataSet(daqData.value, DaqListDaqMap));
      daq.cards = get(daq.cards, 'card', []);
      // @ts-ignore
      ret[daq.name] = daq;
    }, {}));

    this._commit('daqList/update', daqs);
  }

  /**
   * @param {any} data
   */
  onEvent(data) {
    // Only the last event
    /** @type {import("redim-client").XmlData.Data<any>[]} */
    const lastEvent = get(first(data), 'value', []);
    this._commit('events/update', parseDataSet(lastEvent, EACSEventsMap));
  }

  /**
   * @param {any} data
   */
  onTiming(data) {
    debug('timing config fetched');
    this._commit('timing/update', parseDataSet(data, TimingParamsMap));
  }

  /**
   * @param {string} zone
   */
  static getNamespace(zone) {
    return `${zone}_eacs`;
  }

  /**
   * @brief release resources
   */
  destroy() {
    this.close();
    debug(`Unregistering all modules of: ${this.namespace}`);
    const store = this.store;
    this.store = null; // prevent any store interaction
    this.unregisterModule(store, `${this.namespace}/state`);
    this.unregisterModule(store, `${this.namespace}/runInfo`);
    this.unregisterModule(store, `${this.namespace}/daqList`);
    this.unregisterModule(store, `${this.namespace}/timing`);
    this.unregisterModule(store, `${this.namespace}/events`);
    this.unregisterModule(store, this.namespace);
  }

  unregisterModule(store, namespace) {
    if (store && namespace) {
      debug(`Unregistering module: ${namespace}`);
      delete store.sources[namespace];
      store.unregisterModule(namespace.split('/'));
    }
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   * @param {string} dns
   */
  static register(store, zone, dns) {
    debug(`register module eacs`, zone, dns);
    const namespace = EACSSource.getNamespace(zone);
    AutoRegisterSource.registerModule(store, namespace);
    AutoRegisterSource.registerModule(store, `${namespace}/state`);
    AutoRegisterSource.registerModule(store, `${namespace}/runInfo`);
    AutoRegisterSource.registerModule(store, `${namespace}/daqList`);
    AutoRegisterSource.registerModule(store, `${namespace}/timing`);
    AutoRegisterSource.registerModule(store, `${namespace}/events`);
    const instance = new EACSSource(store, zone, dns);
    set(store, [ 'sources', namespace ], instance);
    return instance;
  }
}
