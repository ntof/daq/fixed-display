// @ts-check
import { get, isEmpty, keyBy,  map, set, values } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import axios from 'axios';
import { currentUrl } from '../../utilities';
import { AreaMin } from '../../Consts';
import { DateTime } from 'luxon';
import AutoRegisterSource from './AutoRegisterSource';

/**
 * @typedef {(import('../../interfaces/types').DB.Shift)} Shift
 * @typedef {(import('../../interfaces/types').DB.ShiftInfo)} ShiftInfo
 * @typedef {(import('../../interfaces/types').DB.Association)} Association
 * @typedef {(import('../../interfaces/types').DB.Institute)} Institute
 * @typedef {(import('../../interfaces/types').DB.User)} User
 */

const SHIFTS_UPDATE_INTERVAL = 10 * 60 * 1000; // 10 minutes

/**
 * @brief class for DB interactions
 */
export default class DBSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store) {
    super(store, 'db');
    this.store = store;

    /** @type {NodeJS.Timer?} */
    this.updateShiftsTimer = null;

    this.connect();
  }

  connect() {
    this.updateShiftsTimer = setInterval(this.fetchTodayShifts.bind(this), SHIFTS_UPDATE_INTERVAL);
    this.fetchTodayShifts();
  }

  close() {
    if (this.updateShiftsTimer) {
      clearInterval(this.updateShiftsTimer);
    }
  }

  destroy() {
    this.close();
    super.destroy();
  }

  async fetchTodayShifts() {
    const query = {
      date: {
        '$gte': DateTime.now().startOf('day').toMillis(),
        '$lte': DateTime.now().endOf('day').toMillis()
      }
    };
    // Retrieve Shifts
    const ret = await axios.get(currentUrl() + '/db/shifts',
      { params: { query } }).catch(this.onError);
    const shifts = get(ret, 'data', []);

    // No Shifts
    if (isEmpty(shifts)) {
      this._commit('setTodayShifts', {});
      return;
    }

    // Retrieve Associations
    const assoIds = map(shifts, 'assoId');
    const associations = await this.getAssociations(assoIds);

    // Retrieve Institutes
    const instIds = map(values(associations), 'institute');
    const institutes = await this.getInstitutes(instIds);

    // Retrieve Users
    const userIds = map(values(associations), 'userId');
    const users = await this.getUsers(userIds);

    // Creating ShiftInfo object
    const shiftInfos = map(shifts, (s) => {
      const instituteId = get(associations, [ s.assoId, 'institute' ]);
      const userId = get(associations, [ s.assoId, 'userId' ]);
      set(s, 'institute', institutes[instituteId]);
      set(s, 'user', users[userId]);
      return s;
    });
    this._commit('setTodayShifts', shiftInfos);
  }

  /**
   * @param {Array<number>} assoIds
   * @returns {Promise<{[id: number]: Association}>}
   */
  async getAssociations(assoIds) {
    const query = {
      id: { '$in': assoIds }
    };

    const ret = await axios.get(currentUrl() + '/db/associations',
      { params: { query } }).catch(this.onError);

    return keyBy(get(ret, 'data', []), 'id');
  }

  /**
   * @param {Array<number>} instIds
   * @returns {Promise<{[id: number]: Institute}>}
   */
  async getInstitutes(instIds) {
    const query = {
      id: { '$in': instIds }
    };

    const ret = await axios.get(currentUrl() + '/db/institutes',
      { params: { query } }).catch(this.onError);

    return keyBy(get(ret, 'data', []), 'id');
  }

  /**
   * @param {Array<number>} userIds
   * @returns {Promise<{[id: number]: User}>}
   */
  async getUsers(userIds) {
    const query = {
      id: { '$in': userIds }
    };

    const ret = await axios.get(currentUrl() + '/db/users',
      { params: { query } }).catch(this.onError);

    return keyBy(get(ret, 'data', []), 'id');
  }

  /**
   * @param {number} runNumber
   * @param {string} experiment
   */
  async getTotalProtons(runNumber, experiment) {
    const runNumberMin = (runNumber > AreaMin.EAR2) ? AreaMin.EAR2 : AreaMin.EAR1;
    const from = DateTime.now().startOf('year').toMillis();
    const to = DateTime.now().endOf('year').toMillis();
    const query = { experiment, runNumberMin, runNumber, from, to };

    const ret = await axios.get(currentUrl() + '/db/runs/totalProtons',
      { params: query }).catch(this.onError);

    return get(ret, 'data', 0);
  }

  /**
   * @param {any} err
   */
  onError(err) {
    logger.error(err);
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   */
  static register(store) {
    const module = {
      namespaced: true,
      state: () => ({ todayShifts: {} }),
      mutations: {
        /**
         * @param {AppStore.ShiftState} state
         * @param {Array<ShiftInfo>} shifts
         */
        setTodayShifts: (state, shifts) => {
          state.todayShifts = keyBy(shifts, 'shiftKind');
        }
      }
    };
    DBSource.registerModule(store, 'db', module);
    const instance = new DBSource(store);
    set(store, [ 'sources', 'db' ], instance);
    return instance;
  }
}
