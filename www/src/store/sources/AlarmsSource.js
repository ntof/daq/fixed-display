// @ts-check
import d from 'debug';
import { BaseLogger as logger } from '@cern/base-vue';
import { bindAll, get, isNull, set } from 'lodash';
import { currentUrl, dnsUrl } from '../../utilities';
import { AlarmSeverity } from '../../Consts';
import AutoRegisterSource from './AutoRegisterSource';
// @ts-ignore
import AlarmServerWorker from '../../workers/AlarmServer.shared-worker';

const debug = d('app:store:alarms');

/**
 * @typedef {{
 *   name?: string,
 *   message?: string
 * } | string } ErrWarn
 */

export default class AlarmsSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   * @param {string} dns
   */
  constructor(store, zone, dns) {
    super(store, AlarmsSource.getNamespace(zone));
    this.zone = zone;
    this.dns = dns;
    /** @type {SharedWorker | null} */
    this.worker = null;

    bindAll(this, [ 'onError', 'onMessage', 'setFilters' ]);
    this.connect();
  }

  connect() {
    this.close();
    if (this.dns) {
      debug('connecting to Alarm Server:', this.zone);
      this.worker = new AlarmServerWorker();
      // @ts-ignore
      this.worker.port.start();
      // @ts-ignore
      this.worker.port.onmessage = this.onMessage;

      this.setFilters();

      this.postMessage({
        service: 'Alarms',
        options: { proxy: currentUrl(), dns: dnsUrl(this.dns) }
      });
    }
    else {
      this.onMessage(null);
    }
  }

  close() {
    if (this.worker) {
      this.worker.port.postMessage({ destroy: true });
      this.worker.port.close();
    }
    // @ts-ignore
    this.worker = null;
  }

  destroy() {
    this.close();
    super.destroy();
  }

  /**
   * @param {ErrWarn} err
   */
  onError(err /*: any */) {
    logger.error(err);
  }

  setFilters() {
    if (!this.worker) { return; }

    // Critical + Error + Warnings. Not masked and Not disabled
    const customFilter = {
      offset: 0,
      limit: 1000,
      search: {
        "$and": [
          { "severity": { "$in": [
            AlarmSeverity.CRITICAL,
            AlarmSeverity.ERROR,
            AlarmSeverity.WARNING
          ] } },
          { "masked": { "$eq": false } },
          { "disabled": { "$eq": false } }
        ]
      },
      orderBy: "date:asc"
    };

    this.postMessage(customFilter);
  }

  beforePostMessage() {
    this._commit('update', { loading: true });
  }

  /**
   * @param {any} msg
   */
  postMessage(msg /*: {} */) {
    if (this.worker) {
      this.beforePostMessage();
      this.worker.port.postMessage(msg);
    }
  }

  /**
   * @param {?any} msg
   */
  onMessage(msg /*: any */) {
    if (isNull(msg)) {
      this._commit('update', { alarms: null, stats: null, loading: false });
      return;
    }
    const data = get(msg, 'data');
    if (data.data) {
      this._commit('update', { alarms: data.data, stats: data.stats, loading: false });
    }
    else if (data.error) {
      this._commit('update', { loading: true });
      this.onError(data.error);
    }
  }

  /**
   * @param {string} zone
   */
  static getNamespace(zone) {
    return `${zone}_alarms`;
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   * @param {string} dns
   */
  static register(store, zone, dns) {
    const namespace = AlarmsSource.getNamespace(zone);
    AutoRegisterSource.registerModule(store, namespace);
    const instance = new AlarmsSource(store, zone, dns);
    set(store, [ 'sources', namespace ], instance);
    return instance;
  }

}
