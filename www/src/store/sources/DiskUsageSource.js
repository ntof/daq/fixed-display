// @ts-check
import { BaseLogger as logger } from '@cern/base-vue';
import d from 'debug';
import { forEach, isArray, set } from 'lodash';
import { currentUrl, httpToWs } from '../../utilities';
import AutoRegisterSource from './AutoRegisterSource';

const debug = d('app:store:disk');

export default class DiskUsageSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   */
  constructor(store) {
    super(store, 'disk');
    this.store = store;
    /** @type Array<WebSocket> */
    this.clients = [];

    this.connect();
  }

  connect() {
    try {
      debug('connecting to DiskUsage WebSocket Buffer');
      this.close();
      const WsUrl = httpToWs(`${currentUrl()}/buffers/diskUsage`);
      const buffer = new WebSocket(WsUrl);
      buffer.onerror = () => logger.error('WS connsection error for DiskUsage buffer');
      buffer.onmessage = (event) => {
        const dataJson = JSON.parse(/** @type string */(event.data));
        this.onData(dataJson);
      };
      this.clients.push(buffer);
    }
    catch (error) {
      logger.error(error);
    }
  }

  close() {
    forEach(this.clients, (c) => c.close());
    this.clients = [];
  }

  destroy() {
    this.close();
    super.destroy();
  }

  /**
   * @param {?any} data
   */
  onData(data) {
    if (!data) { return; }
    if (isArray(data)) {
      // First ws message with the history
      this._commit('update', { buffer: data });
    }
    else {
      this._commit('update', { lastBufferData: data });
    }
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   */
  static register(store) {
    DiskUsageSource.registerModule(store, 'disk');
    const instance = new DiskUsageSource(store);
    set(store, [ 'sources', 'disk' ], instance);
    return instance;
  }
}
