// @ts-check
import { BaseLogger as logger } from '@cern/base-vue';
import d from 'debug';
import { forEach, isArray, set } from 'lodash';
import { currentUrl, httpToWs } from '../../utilities';
import AutoRegisterSource from './AutoRegisterSource';

const debug = d('app:store:rfmRate');

export default class RFMRateBufferSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   */
  constructor(store, zone) {
    super(store, RFMRateBufferSource.getNamespace(zone));
    this.zone = zone;
    /** @type Array<WebSocket> */
    this.clients = [];

    this.connect();
  }

  async connect() {
    this.close();
    if (this.zone) {
      try {
        debug('connecting to MERGER Rate WebSocket Buffer:', this.zone);
        const WsUrl = httpToWs(`${currentUrl()}/buffers/${this.zone}/rate`);
        const bufferWs = new WebSocket(WsUrl);
        bufferWs.onerror = () => logger.error(`WS connsection error for Rate buffer on zone ${this.zone}`);
        bufferWs.onmessage = (event) => {
          const dataJson = JSON.parse(/** @type string */(event.data));
          this.onData(dataJson);
        };
        this.clients.push(bufferWs);
      }
      catch (error) {
        logger.error(error);
      }
    }
    else {
      logger.error("Zone not provided to RFMRateBufferSource.");
    }
  }

  close() {
    forEach(this.clients, (c) => c.close());
    this.clients = [];
  }

  destroy() {
    this.close();
    super.destroy();
  }

  /**
   * @param {?any} data
   */
  onData(data) {
    if (!data) { return; }
    if (isArray(data)) {
      // First ws message with the history
      this._commit('update', { buffer: data });
    }
    else {
      this._commit('update', { lastBufferData: data });
    }
  }

  /**
   * @param {string} zone
   */
  static getNamespace(zone) {
    return `${zone}_rfm_rate`;
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   */
  static register(store, zone) {
    const namespace = RFMRateBufferSource.getNamespace(zone);
    RFMRateBufferSource.registerModule(store, namespace);
    const instance = new RFMRateBufferSource(store, zone);
    set(store, [ 'sources', namespace ], instance);
    return instance;
  }
}
