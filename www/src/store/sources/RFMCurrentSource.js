// @ts-check
import d from 'debug';
import { bindAll, forEach, get, set, transform } from 'lodash';
import { BaseLogger as logger } from '@cern/base-vue';
import { DicXmlParams } from '@ntof/redim-client';
import { currentUrl, dnsUrl } from '../../utilities';
import { parseValue } from '../../interfaces';
import { CurrentRunParamsMap, RFMergerRun } from '../../interfaces/rfmerger';
import AutoRegisterSource from './AutoRegisterSource';

const debug = d('app:store:rfm');

export default class RFMCurrentSource extends AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   * @param {string} dns
   */
  constructor(store, zone, dns) {
    super(store, RFMCurrentSource.getNamespace(zone));
    this.zone = zone;
    this.dns = dns;
    /** @type Array<DicXmlParams> */
    this.clients = [];

    bindAll(this, [ 'onError', 'onCurrentData' ]);
    this.connect();
  }

  async connect() {
    this.close();
    if (this.dns) {
      debug('connecting to MERGER/Current on dns:', this.dns);
      const client = new DicXmlParams('MERGER/Current',
        { proxy: currentUrl() }, dnsUrl(this.dns));
      client.on('error', this.onError);
      client.on('value', this.onCurrentData);
      client.promise().catch(this.onError);
      this.clients.push(client);
    }
    else {
      logger.error("Dns not provided to RFMCurrentSource.");
    }
  }

  close() {
    forEach(this.clients, (c) => c.close());
    this.clients = [];
  }

  destroy() {
    this.close();
    super.destroy();
  }

  /**
   * @param {any} err
   */
  onError(err) {
    logger.error(err);
  }

  /**
   * @param {?any} data
   */
  onCurrentData(data) {
    if (!data) { return; }
    const current = this.extractRunFromParams(data);
    this._commit("update", current);
  }

  /**
   * @param {{ [key: string]: string }} value
   * @returns {RFMergerRun}
   */
  extractRunFromParams(value) /*: */ {
    /** @type {{ [index: number]: string }} */
    const vals = transform(value, (ret, val) => {
      // @ts-ignore
      ret[val.index] = val;
    }, []);

    const aRun = new RFMergerRun();
    forEach(CurrentRunParamsMap, (param, name) => {
      set(aRun, name, parseValue(get(vals, [ param.idx, 'value' ], null), param.type));
    });
    return aRun;
  }

  /**
   * @param {string} zone
   */
  static getNamespace(zone) {
    return `${zone}_rfm_current`;
  }

  /**
   * @brief register module
   * @param {V.Store<AppStore.State>} store
   * @param {string} zone
   * @param {string} dns
   */
  static register(store, zone, dns) {
    const namespace = RFMCurrentSource.getNamespace(zone);
    AutoRegisterSource.registerModule(store, namespace);
    const instance = new RFMCurrentSource(store, zone, dns);
    set(store, [ 'sources', namespace ], instance);
    return instance;
  }
}
