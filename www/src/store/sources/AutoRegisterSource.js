// @ts-check
import Vue from 'vue';
import { forEach } from 'lodash';
import d from 'debug';

const debug = d('app:store:registar');

/**
 * @brief generic source with commit and register utility
 */
export default class AutoRegisterSource {
  /**
   * @param {V.Store<AppStore.State>} [store]
   * @param {string} [namespace] store namespace
   */
  constructor(store, namespace) {
    this.store = store ?? null;
    this.namespace = namespace;
  }

  /**
   * @brief release resources
   */
  destroy() {
    debug(`Unregistering module: ${this.namespace}`);
    const store = this.store;
    this.store = null; // prevent any store interaction

    if (store && this.namespace) {
      delete store.sources[this.namespace];
      store.unregisterModule(this.namespace.split('/'));
    }
  }

  /**
   * commit in the store
   * @param  {string} method
   * @param  {any} [arg]
   */
  _commit(method, arg) {
    this.store?.commit(this.namespace ? `${this.namespace}/${method}` : method,
      arg);
  }

  /**
   * @brief remove plugin
   * @param {V.Store<AppStore.State>} store
   * @param {string} namespace
   * @param {V.Module<{}, AppStore.State>?=} module
   */
  static registerModule(store, namespace, module) {
    store.sources?.[namespace]?.destroy();
    const mdef = module ?? {
      namespaced: true,
      state: {},
      mutations: {
        update(state, payload) {
          forEach(payload, (value, key) => {
            Vue.set(state, key, value);
          });
        }
      }
    };
    store.registerModule(namespace.split('/'), mdef);
    debug(`Registering module: ${namespace}`);
  }
}
