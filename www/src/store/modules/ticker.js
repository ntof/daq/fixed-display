// @ts-check
import { DateTime, Settings } from "luxon";
// Configure the time zone
// @ts-ignore
Settings.defaultZoneName = "Europe/Paris";
Settings.defaultLocale = "fr";

const TICKER_INTERVAL = 30 * 1000; // 30 sec in ms

// AutoRefresh
const loadTime = DateTime.local();
const AUTO_REFRESH_INTERVAL = 12 * 60 * 60 * 1000; // 12 hours in ms

/** @type {NodeJS.Timer?} */
let tickerPointer = null;

/** @type {V.Module<AppStore.TickerState>} */
export default {
  namespaced: true,
  state: () => ({ now: DateTime.local() }),
  mutations: {
    /**
     * @param {AppStore.TickerState} state
     */
    refreshTicker(state) {
      state.now = DateTime.local();
      if (state.now.toMillis() - loadTime.toMillis() >  AUTO_REFRESH_INTERVAL) {
        // Refresh everything by reloading the page
        window.location.reload();
      }
    }
  },
  actions: {
    /**
     * @param {any} context
     */
    startTicker(context) {
      if (tickerPointer === null) {
        tickerPointer = setInterval(() => {
          context.commit('refreshTicker');
        }, TICKER_INTERVAL);
      }
    },

    /**
     * @param {any} context
     */
    haltTicker(context) {
      if (tickerPointer) {
        clearInterval(tickerPointer);
        tickerPointer = null;
      }
      context.state.now = null;
    }
  }
};
