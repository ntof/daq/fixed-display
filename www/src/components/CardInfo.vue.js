// @ts-check

import { capitalize } from 'lodash';
import Vue from 'vue';

const component = Vue.extend({
  name: 'CardInfo',
  filters: {
    capitalize: capitalize
  },
  props: {
    title: { type: String, default: '' },
    titleCapitalized: { type: Boolean, default: false },
    subtitle: { type: String, default: '' },
    value: { type: [ String, Number ], default: undefined },
    suffix: { type: String, default: undefined },
    placeholder: { type: String, default: 'UNKNOWN' },
    asBigNum: { type: Boolean, default: false },
    asByteSize: { type: Boolean, default: false },
    cssStyle: { type: String, default: '' }
  }
});
export default component;
