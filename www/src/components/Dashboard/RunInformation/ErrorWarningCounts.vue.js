// @ts-check

import { get } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'ErrorWarningCounts',
  props: {
    zone: { type: String, default: '' }
  },
  computed: {
    ...mapState({
      loading() {
        return get(this.$store.state, [ `${this.zone}_alarms`, 'loading' ]);
      },
      stats() {
        return get(this.$store.state, [ `${this.zone}_alarms`, 'stats' ]);
      }
    }),
    /**
     * @this Instance
     * @returns {number}
     */
    errorsCount() {
      return (this.stats?.critical + this.stats?.error) || 0;
    },
    /**
     * @this Instance
     * @returns {number}
     */
    warningsCount() {
      // TODO add EACS/State warnings?
      return (this.stats?.warning) || 0;
    }
  }
});
export default component;
