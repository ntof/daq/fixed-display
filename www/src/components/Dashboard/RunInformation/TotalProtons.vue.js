// @ts-check

import { get } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';

/**
 * @typedef {V.Instance<typeof component> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'TotalProtons',
  props: {
    zone: { type: String, default: '' }
  },
  /**
   * @return {{
   *   experiment: string,
   *   runNumber: number,
   *   dbTotalProton: number
   * }}
   */
  data() {
    return {
      experiment: '',
      runNumber: 0,
      dbTotalProton: 0
    };
  },
  computed: {
    ...mapState({
      runInfo() {
        return get(this.$store.state, [ `${this.zone}_eacs`, 'runInfo' ]);
      }
    })
  },
  watch: {
    runInfo: {
      immediate: true,
      deep: true,
      handler() {
        this.experiment = this.runInfo?.experiment || '';
        this.runNumber = this.runInfo?.runNumber || 0;
      }
    },
    experiment: {
      immediate: true,
      handler() {
        if (!this.experiment) { return; }
        this.loadProtons(this.runNumber, this.experiment);
      }
    },
    runNumber: {
      immediate: true,
      handler() {
        if (!this.runNumber) { return; }
        this.loadProtons(this.runNumber, this.experiment);
      }
    }
  },
  methods: {
    /**
     * @param {number} runNumber
     * @param {string} experiment
     */
    async loadProtons(runNumber, experiment) {
      if (!runNumber || !experiment) { return; }
      this.dbTotalProton = await this.$store.sources.db.getTotalProtons(runNumber, experiment);
    }
  }
});
export default component;
