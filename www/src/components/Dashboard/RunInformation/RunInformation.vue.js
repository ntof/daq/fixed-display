// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import RunInformationRow from './RunInformationRow.vue';


/**
 * @typedef {V.Instance<typeof component> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'RunInformation',
  components: { RunInformationRow },
  computed: {
    ...mapState('config', [ 'zones' ])
  }
});
export default component;
