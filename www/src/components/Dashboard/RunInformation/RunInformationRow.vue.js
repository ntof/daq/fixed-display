// @ts-check

import { filter, get, map, replace } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import CardInfo from '../../CardInfo.vue';
import TotalProtons from './TotalProtons.vue';
import ErrorWarningCounts from './ErrorWarningCounts.vue';

/**
 * @typedef {import('../../../interfaces/types').EACS.RunInfo} RunInfo
 */

/**
 * @param {Array<string>} daqNames
 * @returns {string}
 */
const daqNamesToList = (daqNames) => {
  return map(daqNames, (daq) => {
    return replace(daq, 'ntofdaq-', '');
  }).join(', ');
};

/**
 * @typedef {V.Instance<typeof component> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'RunInformationRow',
  components: { CardInfo, TotalProtons, ErrorWarningCounts },
  props: {
    zone: { type: String, default: '' }
  },
  computed: {
    ...mapState({
      runInfo() {
        return get(this.$store.state, [ `${this.zone}_eacs`, 'runInfo' ]);
      },
      daqList() {
        return get(this.$store.state, [ `${this.zone}_eacs`, 'daqList' ]);
      }
    }),
    /**
     * @this Instance
     * @returns {string}
     */
    daqs() {
      const activeDaqsNames = map(filter(this.daqList, 'used'), 'name');
      return daqNamesToList(activeDaqsNames);
    },

    /**
     * @this Instance
     * @returns {number | string}
     */
    missedTriggers() {
      if (!this.runInfo) { return ''; }
      return this.runInfo.eventNumber - this.runInfo.validatedNumber;
    }
  }
});
export default component;
