// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import GlobalStateCard from './GlobalStateCard.vue';

const component = Vue.extend({
  name: 'GlobalState',
  components: { GlobalStateCard },
  computed: {
    ...mapState('config', [ 'zones' ])
  }
});
export default component;
