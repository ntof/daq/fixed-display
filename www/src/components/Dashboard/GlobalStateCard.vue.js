// @ts-check

import Vue from 'vue';
import { mapState } from 'vuex';
import { EacsState } from '../../Consts';
import CardInfo from '../CardInfo.vue';
import { get } from 'lodash';

const component = Vue.extend({
  name: 'GlobalStateCard',
  components: { CardInfo },
  props: {
    zone: { type: String, default: '' }
  },
  computed: {
    ...mapState({
      strValue() {
        return get(this.$store.state, [ `${this.zone}_eacs`, 'state', 'strValue' ]);
      },
      value() {
        return get(this.$store.state, [ `${this.zone}_eacs`, 'state', 'value' ]);
      }
    })
  },
  methods: {
    /**
     * @param {number} state
     */
    getCssStyle(state) {
      switch (state) {
      case EacsState.RUNNING:
        return 'bg-success';
      case EacsState.ERROR:
        return 'bg-danger';
      case EacsState.STOPPING:
        return 'bg-warnings';
      default:
        return '';
      }
    }
  }
});
export default component;
