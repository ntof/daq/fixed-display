// @ts-check
import { get, isNil } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import CardInfo from '../CardInfo.vue';
import { AvgWindowMinutes } from '../../Consts';

/**
 * @typedef {{
 *  diskChart: V.Instance<typeof BaseVue.BaseLineChart>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>>} Instance
 */
const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'DiskUsage',
  components: { CardInfo },
  /**
   * @return {{
   *   AvgWindowMinutes: number
   * }}
   */
  data() { return { AvgWindowMinutes }; },
  computed: {
    ...mapState({
      buffer() {
        return get(this.$store.state, [ `disk`, 'buffer' ]);
      },
      lastBufferData() {
        return get(this.$store.state, [ `disk`, 'lastBufferData' ]);
      },
      diskAxisDef() {
        return get(this.$store.state, [ `config`, 'diskAxisDef' ]);
      }
    })
  },
  watch: {
    lastBufferData() {
      if (isNil(this.lastBufferData)) { return; }
      this.$refs.diskChart.add(this.lastBufferData);
    }
  },
  methods: {
    /**
     * @param {number} value
     */
    getCssStyle(value) {
      if (value > 80) { return 'bg-danger'; }
      else if (value > 40) { return 'bg-warning'; }
      else { return 'bg-success'; }
    }
  }
});
export default component;
