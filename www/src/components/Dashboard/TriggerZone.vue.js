// @ts-check

import { get } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import { TriggerModeMap } from '../../Consts';
import CardInfo from '../CardInfo.vue';

const component = Vue.extend({
  name: 'TriggersZone',
  components: { CardInfo },
  props: {
    zone: { type: String, default: '' }
  },
  computed: {
    ...mapState({
      timing() {
        return get(this.$store.state, [ `${this.zone}_eacs`, 'timing' ]);
      }
    }),
    triggerMode() {
      // @ts-ignore it will be 0, 1 or 2
      return TriggerModeMap[this.timing?.mode] ?? 'UNKNOWN';
    }
  }
});
export default component;
