// @ts-check

import { first, get, isNil } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import CardInfo from '../CardInfo.vue';
import TriggerZone from './TriggerZone.vue';

const component = Vue.extend({
  name: 'Triggers',
  components: { CardInfo, TriggerZone },
  computed: {
    ...mapState('config', [ 'zones' ]),
    ...mapState({
      lastEvent() {
        if (isNil(this.zones)) { return; }
        // Bct is common everywhere, just take the info from the first zone
        return get(this.$store.state, [ `${first(this.zones).name}_eacs`, 'events' ]);
      }
    }),
    protons() {
      return this.lastEvent?.protons ?? 0;
    }
  }
});
export default component;
