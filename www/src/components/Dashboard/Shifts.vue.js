// @ts-check

import { first, get } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import { ShiftKindType } from '../../Consts';
import CardInfo from '../CardInfo.vue';

/**
 * @typedef {V.Instance<typeof component> } Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'Shifts',
  components: { CardInfo },
  /**
   * @return {{
   *   ShiftKindType: typeof ShiftKindType,
   *   activeShift: string | null
   * }}
   */
  data() { return { ShiftKindType, activeShift: null }; },
  computed: {
    ...mapState({
      todayShifts() {
        return get(this.$store.state, [ 'db', 'todayShifts' ]);
      }
    }),
    ...mapState('ticker', [ 'now' ])
  },
  watch: {
    now() {
      this.activeShift = this.getActiveShift();
    }
  },
  /** @this Instance */
  beforeMount() {
    this.activeShift = this.getActiveShift();
  },
  methods: {
    /**
     * @param {string} shiftKind
     */
    getShiftInfo(shiftKind) {
      const shift = get(this.todayShifts, shiftKind);
      if (!shift) { return 'NO INFO'; }
      return `${this.userInfo(shift.user)} ${this.instInfo(shift.institute)}`;
    },
    /**
     * @param {{ firstName: string; lastName: string; } | undefined } userObj
     */
    userInfo: (userObj) => {
      if (!userObj) { return 'Empty'; }
      return `${first(userObj.firstName)}. ${userObj.lastName}`;
    },
    /**
     * @param {{ alias: string; } | undefined } instObj
     */
    instInfo: (instObj) => {
      if (!instObj) { return ''; }
      return `(${instObj.alias})`;
    },
    getActiveShift() {
      if (this.now.hour >= 0 && this.now.hour < 8) {
        return ShiftKindType.NIGHT;
      }
      else if (this.now.hour >= 8 && this.now.hour < 16) {
        return ShiftKindType.MORNING;
      }
      else {
        return ShiftKindType.AFTERNOON;
      }
    },
    /**
     * @param {string} shiftKind
     */
    getCssIFShiftActive(shiftKind) {
      if (shiftKind === this.activeShift) {
        return 'bg-warning';
      }
      else {
        return '';
      }
    }
  }
});
export default component;
