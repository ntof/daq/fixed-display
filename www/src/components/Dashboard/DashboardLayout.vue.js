// @ts-check

import Vue from 'vue';
import Triggers from './Triggers.vue';
import GlobalState from './GlobalState.vue';
import RunInformation from './RunInformation/RunInformation.vue';
import Shifts from './Shifts.vue';
import DiskUsage from './DiskUsage.vue';
import { mapState } from 'vuex';
import { forEach } from 'lodash';
import DiskUsageSource from '../../store/sources/DiskUsageSource';
import DBSource from '../../store/sources/DBSource';
import EACSSource from '../../store/sources/EACSSource';
import AlarmsSource from '../../store/sources/AlarmsSource';

const component = Vue.extend({
  name: 'DashboardLayout',
  components: { Triggers, GlobalState, RunInformation, Shifts, DiskUsage },
  computed: {
    ...mapState('config', [ 'zones' ])
  },
  beforeMount() {
    this.start();
  },
  beforeDestroy() {
    this.stop();
  },
  methods: {
    async start() {
      this.stop();
      const store = this.$store;
      const config = await store.sources.config.waitForFetch();
      this.$options.sources.push(DiskUsageSource.register(store));
      this.$options.sources.push(DBSource.register(store));
      forEach(config.zones, (zone) => {
        this.$options.sources.push(EACSSource.register(store, zone.name, zone.dns));
        this.$options.sources.push(AlarmsSource.register(store, zone.name, zone.dns));
      });
    },
    stop() {
      forEach(this.$options.sources, (source) => {
        source.destroy();
      });
      this.$options.sources = [];
    }
  }
});
export default component;
