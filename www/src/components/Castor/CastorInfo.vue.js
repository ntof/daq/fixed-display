// @ts-check


import { get } from 'lodash';
import Vue from 'vue';
import { mapState } from 'vuex';
import RFMStats from './RFMStats.vue';

/**
 * @typedef {V.Instance<typeof component>} Instance
 */
const component = /** @type {V.Constructor<any, any>} */ (Vue).extend({
  name: 'CastorInfo',
  components: { RFMStats },
  props: {
    dns: { type: String, default: '' },
    zone: { type: String, default: '' }
  },
  computed: {
    ...mapState({
      current() {
        return get(this.$store.state, [ `${this.zone}_rfm_current` ]);
      }
    }),
    /**
     * @this Instance
     * @returns {any}
     */
    runNumber() {
      return get(this.current, 'runNumber', null);
    }
  }
});
export default component;
