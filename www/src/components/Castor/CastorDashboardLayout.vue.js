// @ts-check

import Vue from 'vue';
import CastorInfo from './CastorInfo.vue';
import { mapState } from 'vuex';
import { forEach } from 'lodash';
import RFMCurrentSource from '../../store/sources/RFMCurrentSource';
import RFMRateBufferSource from '../../store/sources/RFMRateBufferSource';

const component = Vue.extend({
  name: 'CastorDashboardLayout',
  components: { CastorInfo },
  computed: {
    ...mapState('config', [ 'zones' ])
  },
  mounted() {
    this.start();
  },
  beforeDestroy() {
    this.stop();
  },
  methods: {
    async start() {
      this.stop();
      const store = this.$store;
      const config = await store.sources.config.waitForFetch();
      forEach(config.zones, (zone) => {
        this.$options.sources.push(RFMCurrentSource.register(store, zone.name, zone.dns));
        this.$options.sources.push(RFMRateBufferSource.register(store, zone.name));
      });
    },
    stop() {
      forEach(this.$options.sources, (source) => {
        source.destroy();
      });
      this.$options.sources = [];
    }
  }
});
export default component;
