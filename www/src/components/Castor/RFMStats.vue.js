// @ts-check
import Vue from 'vue';
import { get, isNil, pick } from 'lodash';
import CardInfo from '../CardInfo.vue';
import { UrlUtilitiesMixin } from '../../utilities';
import { mapState } from 'vuex';
import { AvgWindowMinutes } from '../../Consts';

// attributes coming from MERGER/Current that we want to show as stats
const STATS_TO_SHOW = [ 'incomplete', 'waiting', 'transferring', 'copied', 'migrated',
  'failed', 'transferred', 'rate' ];

/**
 * @typedef {{ timestamp: number,  rate: number, rateAvg: number | null }} RateDataPoint
 */

/**
 * @typedef {{
 *  rateChart: V.Instance<typeof BaseVue.BaseLineChart>
 * }} Refs
 * @typedef {V.Instance<typeof component, V.ExtVue<any, Refs>>
 *  & V.Instance<typeof UrlUtilitiesMixin>} Instance
 */

const component = /** @type {V.Constructor<any, Refs>} */ (Vue).extend({
  name: 'RFMStats',
  components: { CardInfo },
  mixins: [
    UrlUtilitiesMixin
  ],
  props: {
    zone: { type: String, default: '' }
  },
  /**
   * @return {{
   *   AvgWindowMinutes: number
   * }}
   */
  data() { return { AvgWindowMinutes }; },
  computed: {
    ...mapState({
      current() {
        return get(this.$store.state, [ `${this.zone}_rfm_current` ]);
      },
      buffer() {
        return get(this.$store.state, [ `${this.zone}_rfm_rate`, 'buffer' ]);
      },
      lastBufferData() {
        return get(this.$store.state, [ `${this.zone}_rfm_rate`, 'lastBufferData' ]);
      }
    }),
    /**
     * @this Instance
     * @returns {any}
     */
    stats() {
      return pick(this.current, STATS_TO_SHOW);
    }
  },
  watch: {
    lastBufferData() {
      if (isNil(this.lastBufferData)) { return; }
      this.$refs.rateChart.add(this.lastBufferData);
    }
  }
});
export default component;
