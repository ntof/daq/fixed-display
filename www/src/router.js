// @ts-check
import Vue from 'vue';
import Router from 'vue-router';

import DashboardLayout from './components/Dashboard/DashboardLayout.vue';
import CastorDashboardLayout from './components/Castor/CastorDashboardLayout.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    { name: 'Dashboard', path: '/', component: DashboardLayout },
    { name: 'Castor', path: '/castor', component: CastorDashboardLayout },

    { path: '/index.html', redirect: '/' },
    { path: '/Layout', redirect: '/' }
  ]
});

export default router;
