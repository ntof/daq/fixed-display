// @ts-check

/** @enum {{idx: number, str: string}} */
export const SeverityMap = {
  NONE: { idx: 0, str: "NONE" },
  INFO: { idx: 0, str: "INFO" },
  WARNING: { idx: 0, str: "WARNING" },
  ERROR: { idx: 0, str: "ERROR" },
  CRITICAL: { idx: 0, str: "CRITICAL" }
};

export class Alarm {
  /**
   * @details Class that store all information of an Alarm
   * @param {string} ident
   */
  constructor(ident) {
    /** @type {string} */
    this.ident = ident;
    /** @type {number} */
    this.severity = SeverityMap.NONE.idx;
    /** @type {string} */
    this.severityStr = SeverityMap.NONE.str;
    /** @type {?number} */
    this.date = null; // lastActive
    /** @type {boolean} */
    this.active = false;
    /** @type {boolean} */
    this.masked = false;
    /** @type {boolean} */
    this.disabled = false;
    /** @type {string} */
    this.system = '';
    /** @type {string} */
    this.subsystem = '';
    /** @type {string} */
    this.source = '';
    /** @type {string} */
    this.message = '';
  }
}
