// @ts-check

import { OutputMode, TimingMode } from "./timing";
import { Alarm as AlarmObj } from "./alarmserver";
import { XmlData } from "@ntof/redim-client";

export = Iface
export as namespace Iface

declare namespace Iface {

  /**
   * XmlData
   */
  namespace XML {
    interface ParamDesc {
      index?: number,
      type: XmlData.DataType
      map?: ParamMap
      field?: string
    }

    export interface ParamMap {
      [key: string]: ParamDesc
    }
  }

  namespace Config {
    interface Zone {
      name: string;
      dns: string;
      runNumberMin: number;
      runNumberMax: number;
      servers: Array<{ name: string, server: string; }>
    }
  }

  /**
   * DB
   */
  namespace DB {

    interface Shift {
      id: number;
      date: number;
      assoId: number;
      shiftKind: string;
    }

    interface Association {
      id: number,
      operYear: number,
      userId: number,
      institute: number
    }

    interface Institute {
      id: number,
      name: string,
      city: string,
      country: string,
      alias: string,
      status: number
    }

    interface User {
      id: string,
      login: string,
      firstName: string,
      lastName: string,
      email: string,
      status: boolean,
      privileges: string[]
    }

    type ShiftInfo = Shift & {
      institute: Institute;
      user: User;
    }
  }

  /**
   * EACS
   */
  namespace EACS {
    interface DaqListCardInfo {
      channelsCount: number;
      type: string;
      serialNumber: string;
      index: number;
      used: boolean;
      daq: string;
    }

    interface DaqListInfo {
      name: string;
      used: boolean;
      cards: Array<DaqListCardInfo>;
    }

    interface RunInfo {
      runNumber?: number;
      eventNumber?: number;
      validatedNumber?: number;
      validatedProtons?: number;
      title?: string;
      description?: string;
      area?: string;
      experiment?: string;
      detectorsSetupId?: number;
      materialsSetupId?: number;
    }

    interface EventData {
      beamType: number;
      cyclestamp: number;
      eventNumber: number;
      periodNB: number;
      protons?: number;
      validatedNumber: number;
    }
  }

  /**
   * Alarms Types
   */
  namespace AlarmServer {
    type Alarm = AlarmObj;

    interface Filters {
      offset: number;
      limit: number;
      search?: { [key: string]: string } | null;
      orderBy?: string | null; // suffix with :asc/:desc
    }

    interface Stats {
      critical: number;
      error: number;
      warning: number;
      info: number;
      none: number;
      masked: number;
      disabled: number;
      active: number;
    }
  }

  /**
   * Timing Types
   */
  namespace Timing {
    interface TimingData {
      mode: TimingMode;
      triggerRepeat: number;
      triggerPeriod: number;
      triggerPause: number;
      eventNumber: number;
      calibOut: OutputMode;
      parasiticOut: OutputMode;
      primaryOut: OutputMode;
    }
  }
}


