// @ts-check
import { ParamType } from './index';

/**
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/** @enum {number} */
export const TimingMode = {
  DISABLED: 0,
  AUTOMATIC: 1,
  CALIBRATION: 2
};
/** @enum {number} */
export const OutputMode = {
  DISABLED: 0,
  ENABLED: 1
};

/** @type ParamMap */
export const TimingParamsMap = {
  mode: { index: 1, type: ParamType.ENUM },
  triggerRepeat: { index: 2, type: ParamType.INT32 },
  triggerPeriod: { index: 3, type: ParamType.INT32 },
  triggerPause: { index: 4, type: ParamType.INT32 },
  eventNumber: { index: 5, type: ParamType.INT64 },
  calibOut: { index: 6, type: ParamType.ENUM },
  parasiticOut: { index: 7, type: ParamType.ENUM },
  primaryOut: { index: 8, type: ParamType.ENUM }
};
