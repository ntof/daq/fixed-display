// @ts-check
import { ParamType } from "./index";

/**
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/** @type ParamMap */
export const RunInfoMap = {
  runNumber: { index: 1, type: ParamType.UINT32 },
  eventNumber: { index: 2, type: ParamType.UINT32 },
  validatedNumber: { index: 3, type: ParamType.UINT32 },
  validatedProtons: { index: 4, type: ParamType.DOUBLE },
  title: { index: 5, type: ParamType.STRING },
  description: { index: 6, type: ParamType.STRING },
  area: { index: 7, type: ParamType.STRING },
  experiment: { index: 8, type: ParamType.STRING },
  detectorsSetupId: { index: 9, type: ParamType.INT64 },
  materialsSetupId: { index: 10, type: ParamType.INT64 }
};

/** @type ParamMap */
export const EventsEventMap = {
  eventNumber: { index: 0, type: ParamType.UINT32 },
  validatedNumber: { index: 1, type: ParamType.UINT32 },
  beamType: { index: 2, type: ParamType.STRING },
  protons: { index: 3, type: ParamType.DOUBLE },
  cyclestamp: { index: 4, type: ParamType.INT64 },
  periodNB: { index: 5, type: ParamType.INT32 }
};

export const EventsBeamTypes = {
  1: "CALIBRATION",
  2: "PRIMARY",
  3: "PARASITIC"
};

/** @enum {number} */
export const State = {
  LOADING: 1,
  IDLE: 2,
  FILTERS_INIT: 3,
  SAMPLES_INIT: 4,
  COLLIMATOR_INIT: 5,
  HIGHVOLTAGE_INIT: 6,
  DAQS_INIT: 7,
  MERGER_INIT: 8,
  STARTING: 9,
  RUNNING: 10,
  STOPPING: 11,
  ERROR: -2
};

/** @enum {string} */
export const DataStatus = {
  UNKNOWN: "stopped",
  BAD: "bad",
  OK: "ok"
};

/** @type ParamMap */
export const DaqListCardMap = {
  channelsCount: { index: 0, type: ParamType.UINT32 },
  type: { index: 1, type: ParamType.STRING },
  serialNumber: { index: 2, type: ParamType.STRING },
  used: { index: 3, type: ParamType.BOOL }
};

/** @type ParamMap */
export const DaqListDaqMap = {
  name: { index: 0, type: ParamType.STRING },
  cards: {
    index: 1, type: ParamType.NESTED,
    map: { card: { type: ParamType.NESTED, map: DaqListCardMap } }
  },
  used: { index: 2, type: ParamType.BOOL }
};

/** @type ParamMap */
export const EACSEventsMap = {
  eventNumber: { index: 0, type: ParamType.UINT32 },
  validatedNumber: { index: 1, type: ParamType.UINT32 },
  beamType: { index: 2, type: ParamType.ENUM },
  protons: { index: 3, type: ParamType.FLOAT },
  cyclestamp: { index: 4, type: ParamType.INT64 },
  periodNB: { index: 5, type: ParamType.INT32 }
};

/** @type ParamMap */
export const SystemSummaryMap = {
  diskUsed: { index: 0, type: ParamType.DOUBLE },
  ramUsed: { index: 1, type: ParamType.DOUBLE },
  cpuUsed: { index: 2, type: ParamType.DOUBLE }
};
