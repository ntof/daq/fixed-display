// @ts-check

import { get, isNil, isObject, keyBy, toNumber, transform } from 'lodash';
import { DicXmlDataSet } from '@ntof/redim-client';

// @ts-ignore
export const ParamType = DicXmlDataSet.DataType;

/**
 * @typedef {import('./types').XML.ParamDesc} ParamDesc
 * @typedef {import('./types').XML.ParamMap} ParamMap
 */

/**
 * @param {string | number} value
 * @param {redim.XmlData.DataType} type
 * @return {boolean|number|string}
 */ // eslint-disable-next-line complexity
export function parseValue(value, type) {
  switch (type) {
  case ParamType.INT64:
  case ParamType.INT32:
  case ParamType.INT16:
  case ParamType.INT8:
  case ParamType.DOUBLE:
  case ParamType.FLOAT:
  case ParamType.ENUM:
  case ParamType.UINT32:
  case ParamType.UINT64:
  case ParamType.UINT8:
  case ParamType.UINT16:
    return toNumber(value);
  case ParamType.BOOL:
    return toNumber(value) > 0;
  default:
    return value;
  }
}

/**
 *
 * @param {any} value
 * @param {ParamDesc} desc
 * @returns {boolean|number|string|*}
 */
function parseParamValue(value, desc) {
  if (desc.type === ParamType.NESTED && desc.map) {
    return parseDataSet(value, desc.map);
  }
  else {
    return parseValue(value, desc.type);
  }
}

/**
 * @brief parse an incoming list of DIMData and shape it as an object
 * @param {Array<redim.XmlData.Data>} data
 * @param {ParamMap} paramMap
 * @returns Object<string, any>
 */
export function parseDataSet(data, paramMap) {
  const indexed = keyBy(data, 'index');
  return transform(paramMap,
    /**
     * @param {Object<string, any>} ret
     * @param {ParamDesc} param
     * @param {string} name
     */
    (ret, param, name) => {
      if (isNil(param.index)) {
      /* collect all using their name */ /* @ts-ignore */
        ret[name] = transform(indexed,
          /**
           * @param {Array<string>} ret
           * @param {redim.XmlData.Data} data
           */
          (ret, data) => {
            if (data.name === name) {
              const item = parseParamValue(data.value, param);
              // Some objects use index as a value
              if (isObject(item)) {
                // @ts-ignore isObject
                item.index = data.index;
              }
              ret.push(item);
            }
          }, []);
      }
      else {
        var value = get(indexed, [ param.index, 'value' ]);
        if (value !== undefined) {
          ret[name] = parseParamValue(value, param);
        }
      }
    }, {});
}
