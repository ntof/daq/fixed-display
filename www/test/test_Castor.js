// @ts-check

import './karma_index';
import { createLocalVue, createRouter, waitForValue } from './utils';
import { setCurrentUrl } from '../src/utilities';
import { butils } from '@cern/base-vue';
import { afterEach, beforeEach, describe, it } from 'mocha';
import { mount } from '@vue/test-utils';
// @ts-ignore
import server from '@cern/karma-server-side';
import App from '../src/App.vue';

describe('Castor', function() {
  /** @type {Tests.Wrapper} */
  let wrapper;

  beforeEach(function() {
    return server.run(function() {
      const utils = serverRequire('./test/server_utils');
      const { NTOFStub } = serverRequire('@ntof/ntof-stubs');
      const { makeDeferred } = serverRequire('@cern/prom');
      this.env = /** @type {any} */({});

      var def = makeDeferred();
      Promise.resolve()
      .then(() => {
        // Start Stub
        this.env.stub = new NTOFStub();
        return this.env.stub.init();
      })
      .then(() => {
        // Start Server
        return utils.createApp(this.env);
      })
      .then(() => {
        var addr = this.env.server.address();
        def.resolve({
          server: this.env.server,
          proxyUrl: `http://localhost:${addr.port}`,
          host: `localhost:${addr.port}`
        });
      });
      return def.promise;
    })
    .then(async (/** @type {any} */ ret) => {
      butils.setCurrentUrl(ret.proxyUrl);
      setCurrentUrl(ret.proxyUrl);

      await server.run(function() {
        // @ts-ignore
        this.env.server?.connectBuffers();
      });

    });
  });

  afterEach(async function() {
    await server.run(function() {
      // @ts-ignore
      this.env.server?.close();
      // @ts-ignore
      this.env.stub?.close();
      this.env = null;
    });
  });

  afterEach(function() {
    if (wrapper) {
      wrapper.destroy();
      // @ts-ignore
      wrapper = null;
    }
  });

  it('can mount Castor and all his components', async function() {
    wrapper = mount(App, { router: createRouter('/castor'),
      localVue: createLocalVue({ auth: false }) });

    await waitForValue(
      () => wrapper.findComponent({ name: 'CastorInfo' }).exists(), true,
      "CastorInfo not loaded", 2000);
    await waitForValue(
      () => wrapper.findComponent({ name: 'RFMStats' }).exists(), true,
      "RFMStats not loaded", 2000);
    await waitForValue(
      () => wrapper.findComponent({ name: 'RFMergerFilesView' }).exists(), true,
      "RFMergerFilesView not loaded", 2000);
  });

});
