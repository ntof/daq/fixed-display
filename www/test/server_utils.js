
const
  { makeDeferred } = require('@cern/prom'),

  Server = require('../../src/Server');

const testConfig = {
  title: "Fixed-Display",
  port: 0, basePath: '',
  db: {
    client: 'sqlite3', useNullAsDefault: true,
    connection: { filename: ':memory:' }
  },
  fixedDisplay: {
    zones: [
      {
        name: "LOCALHOST",
        dns: "localhost",
        servers: [
          {
            name: "LOCALHOST",
            host: "localhost"
          }
        ]
      }
    ]
  }
};

/**
 * @param  {any} env
 */
function createApp(env) {
  var def = makeDeferred();
  env.server = new Server(testConfig);

  env.server.listen(() => def.resolve(undefined));
  return def.promise;
}

module.exports = { testConfig, createApp };
