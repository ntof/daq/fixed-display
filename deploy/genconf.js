#!/usr/bin/env node

const fs = require('fs'),
  path = require('path'),
  { dump, load, preload, schema } = require('@cern/js-yaml'),
  cmd = require('commander'),
  { find, merge, get } = require('lodash');

const opts = cmd.command('genconf')
.argument('<deployment>', 'deployment YAML file to modify')
.argument('[config...]', 'configuration files to inject')
.parse(process.argv);

process.on('unhandledRejection', function(reason) {
  console.warn(reason);
  process.exitCode = 1;
});

const outFile = opts.processedArgs[0];
const inFiles = opts.processedArgs[1];

const confFiles = {};
inFiles.forEach((conf) => {
  confFiles[path.basename(conf)] = preload(fs.readFileSync(conf));
});

const output = load(fs.readFileSync(outFile), { schema });
const config = find(get(output, [ 'items' ]), { kind: 'ConfigMap' });

if (!config) {
  throw new Error('failed to find ConfigMap in: ' + outFile);
}
config.data  = merge(config.data, confFiles);
console.log(dump(output));
